# FICHES_SYMFONY

FICHES_SYMFONY est une application web ayant pour but de dématérialiser les fiches interventions de l'entreprise MICROFUN88 et gérer une base de données de leur clients et PC dépannés.

# TEST LOCAL
Pour lancer le projet sur le serveur local de symfony, créer un nouveau projet Symfony et copier le dossier vendor dans le clone git de ce repository.

## Environnement de développement

### Pré-requis

 * PHP 7.4
 * Composer
 * Symfony CLI
 * Docker
 * Docker-compose

 Vous pouvez vérifier les pré-requis (saud Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

copier dépôt git 

```bash
composer require
npm install
npm run build
symfony console d:m:m
(sudo) docker-compose up -d    //lancer servise docker
symfony serve -d               //lancer serveur dev
symfony serve:stop             //stop serv dev
```

URL SERVER : 127.0.0.1
    phpmyadmin : -p:8080 
    mail : symfony_profiler->server_parameters
