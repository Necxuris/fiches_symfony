--  supprime les services des doublons
delete from records_service where records_fixing_id in (select t1.id from records_fixing t1 inner join records_fixing t2 where t1.id < t2.id and t1.receipt_number!= 0 and t1.receipt_number = t2.receipt_number);

-- supprime les doublons dans records_fixing

delete t1 from records_fixing t1 inner join records_fixing t2 where t1.id < t2.id and t1.receipt_number != 0 and t1.receipt_number = t2.receipt_number;


-- vérificartion

select receipt_number, count(receipt_number) from records_fixing group by receipt_number having count(receipt_number) >1;
select receipt_number, count(receipt_number) from records_fixing group by receipt_number having count(receipt_number) =1;
