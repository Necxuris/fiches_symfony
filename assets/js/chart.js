import Chart from 'chart.js/auto/auto'

let jsonMoMoy = JSON.parse(document.querySelector('.js-json-mo-moy').dataset.jsonMoMoy);
let jsonMoy = JSON.parse(document.querySelector('.js-json-moy').dataset.jsonMoy);
let jsonMo = JSON.parse(document.querySelector('.js-json-mo').dataset.jsonMo);
let jsonMat = JSON.parse(document.querySelector('.js-json-mat').dataset.jsonMat);
let jsonCrowd = JSON.parse(document.querySelector('.js-json-crowd').dataset.jsonCrowd);
let jsonTop = JSON.parse(document.querySelector('.js-json-top').dataset.jsonTop);

let chartMoMoy = new Chart(
    document.getElementById('chartMoMoy'), {
        type: 'line',
        data: {
            labels: jsonMoMoy.labels,
            datasets: [{
                label: 'Mo moyen',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: jsonMoMoy.data,
            }]
        }
    }
);

let chartMoy = new Chart(
    document.getElementById('chartMoy'), {
        type: 'line',
        data: {
            labels: jsonMoy.labels,
            datasets: [{
                label: 'Panier moyen',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: jsonMoy.data,
            }]
        }
    }
);

let chartMo = new Chart(
    document.getElementById('chartMo'), {
        type: 'line',
        data: {
            labels: jsonMo.labels,
            datasets: [{
                label: "Main d\'oeuvre",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: jsonMo.data,
            }]
        }
    }
);

let chartMat = new Chart(
    document.getElementById('chartMat'), {
        type: 'line',
        data: {
            labels: jsonMat.labels,
            datasets: [{
                label: 'Vente matériel',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: jsonMat.data,
            }]
        }
    }
);

let chartCrowd = new Chart(
    document.getElementById('chartCrowd'), {
        type: 'bar',
        data: {
            labels: jsonCrowd.labels,
            datasets: [{
                label: 'Nombre de nouveau dépannage',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: jsonCrowd.data,
            }]
        }
    }
);

let chartTop = new Chart(
    document.getElementById('chartTop'), {
        type: 'bar',
        data: {
            labels: jsonTop.labels,
            datasets: [{
                label: 'Top forfait',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: jsonTop.data,
            }]
        }
    }
);

function modifyChartsAndStats(data) {
    data = JSON.parse(data);

    data.charts.jsonMat = JSON.parse(data.charts.jsonMat);
    data.charts.jsonMo = JSON.parse(data.charts.jsonMo);
    data.charts.jsonMoy = JSON.parse(data.charts.jsonMoy);
    data.charts.jsonMoMoy = JSON.parse(data.charts.jsonMoMoy);
    data.charts.jsonCrowd = JSON.parse(data.charts.jsonCrowd);
    data.topServices = JSON.parse(data.topServices);

    chartCrowd.data.labels = data.charts.jsonCrowd.labels;
    chartCrowd.data.datasets.forEach(dataset => {
        dataset.data = data.charts.jsonCrowd.data;
    });
    chartCrowd.update();
    chartMo.data.labels = data.charts.jsonMo.labels;
    chartMo.data.datasets.forEach(dataset => {
        dataset.data = data.charts.jsonMo.data;
    });
    chartMo.update();
    chartMoy.data.labels = data.charts.jsonMoy.labels;
    chartMoy.data.datasets.forEach(dataset => {
        dataset.data = data.charts.jsonMoy.data;
    });
    chartMoy.update();
    chartMat.data.labels = data.charts.jsonMat.labels;
    chartMat.data.datasets.forEach(dataset => {
        dataset.data = data.charts.jsonMat.data;
    });
    chartMat.update();
    chartMoMoy.data.labels = data.charts.jsonMoMoy.labels;
    chartMoMoy.data.datasets.forEach(dataset => {
        dataset.data = data.charts.jsonMoMoy.data;
    });
    chartMoMoy.update();
    chartTop.data.labels = data.topServices.labels;
    chartTop.data.datasets.forEach(dataset => {
        dataset.data = data.topServices.data;
    });
    chartTop.update();

    document.getElementById("client").innerText = data.stats.client.new + " ";
    let spanClient = document.createElement("span");
    if (data.stats.client.rate > 0) {
        spanClient.className = "text-success text-sm font-weight-bolder";
        spanClient.innerHTML = "+";
    } else
        spanClient.className = "text-danger text-sm font-weight-bolder";
    spanClient.innerHTML += data.stats.client.rate + "%";
    document.getElementById("client").appendChild(spanClient);

    document.getElementById("fixing").innerText = data.stats.fixing.new + " ";
    let spanFixing = document.createElement("span");
    if (data.stats.fixing.rate > 0) {
        spanFixing.className = "text-success text-sm font-weight-bolder";
        spanFixing.innerHTML = "+";
    } else
        spanFixing.className = "text-danger text-sm font-weight-bolder";
    spanFixing.innerHTML += data.stats.fixing.rate + "%";
    document.getElementById("fixing").appendChild(spanFixing);

    document.getElementById("labor").innerText = data.stats.labor.new + "€ ";
    let spanLabor = document.createElement("span");
    if (data.stats.labor.rate > 0) {
        spanLabor.className = "text-success text-sm font-weight-bolder";
        spanLabor.innerHTML = "+";
    } else
        spanLabor.className = "text-danger text-sm font-weight-bolder";
    spanLabor.innerHTML += data.stats.labor.rate + "%";
    document.getElementById("labor").appendChild(spanLabor);

    document.getElementById("total").innerText = data.stats.total.new + "€ ";
    let spanTotal = document.createElement("span");
    if (data.stats.total.rate > 0) {
        spanTotal.className = "text-success text-sm font-weight-bolder";
        spanTotal.innerHTML = "+";
    } else
        spanTotal.className = "text-danger text-sm font-weight-bolder";
    spanTotal.innerHTML += data.stats.total.rate + "%";
    document.getElementById("total").appendChild(spanTotal);

    document.getElementById("material").innerText = data.stats.material.new + "€ ";
    let spanMaterial = document.createElement("span");
    if (data.stats.material.rate > 0) {
        spanMaterial.className = "text-success text-sm font-weight-bolder";
        spanMaterial.innerHTML = "+";
    } else
        spanMaterial.className = "text-danger text-sm font-weight-bolder";
    spanMaterial.innerHTML += data.stats.material.rate + "%";
    document.getElementById("material").appendChild(spanMaterial);

    document.getElementById("moy").innerText = Math.round(data.stats.moy.new) + "€ ";
    let spanMoy = document.createElement("span");
    if (data.stats.moy.rate > 0) {
        spanMoy.className = "text-success text-sm font-weight-bolder";
        spanMoy.innerHTML = "+";
    } else
        spanMoy.className = "text-danger text-sm font-weight-bolder";
    spanMoy.innerHTML += Math.round(data.stats.moy.rate) + "%";
    document.getElementById("moy").appendChild(spanMoy);

    document.getElementById("laborMoy").innerText = Math.round(data.stats.laborMoy.new) + "€ ";
    let spanLaborMoy = document.createElement("span");
    if (data.stats.laborMoy.rate > 0) {
        spanLaborMoy.className = "text-success text-sm font-weight-bolder";
        spanLaborMoy.innerHTML = "+";
    } else
        spanLaborMoy.className = "text-danger text-sm font-weight-bolder";
    spanLaborMoy.innerHTML += Math.round(data.stats.laborMoy.rate) + "%";
    document.getElementById("laborMoy").appendChild(spanLaborMoy);
}

function changeStats() {
    console.log(this.value);
    let json = JSON.stringify({'date' : this.value});
    let xml = new XMLHttpRequest();
    xml.open("POST", window.location.href + "/change-stats");
    xml.setRequestHeader("ContentType", "application/json");
    xml.onreadystatechange = function() {
        if(xml.readyState == 4 && xml.status == 200) {
            modifyChartsAndStats(xml.responseText);
        }
    }
    xml.send(json);
}

listDate = document.getElementById("listDate");
listDate.addEventListener('change', changeStats);