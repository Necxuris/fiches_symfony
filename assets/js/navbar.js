function createNavbar() {
    url = window.location.pathname.split('/');
    let navbar = document.querySelector("#js-navbar");
    length = url.length;
    var reg = /^\d+$/;
    url.forEach((path, key) => {
        if (path != '' && path[0] == reg.test())
            return;
        let li = document.createElement('li');
        li.className = 'breadcrumb-item text-sm';
        let a = document.createElement('a');
        a.className = "opacity-5 text-dark";
        switch (path) {
            case '':
                a.innerHTML = 'Home';
                a.href = window.location.origin;
                break;
            case 'fix':
                a.innerHTML = 'Dépannage';
                a.href = navbar.lastElementChild.firstChild.href + 'fix/';
                break;
            case 'fixing':
                a.innerHTML = 'Dashboard';
                a.href = navbar.lastElementChild.firstChild.href + 'fixing/' + url[key+1];
                url.splice(key + 1, 1);
                break;
            case 'backoffice':
                a.innerHTML = 'Backoffice';
                a.href = navbar.lastElementChild.firstChild.href + 'backoffice/';
                break;
            case 'employee':
                a.innerHTML = 'Salarié';
                a.href = navbar.lastElementChild.firstChild.href + 'employee/';
                break;
            case 'material':
                a.innerHTML = 'Matériel';
                a.href = navbar.lastElementChild.firstChild.href + 'material/';
                break;
            case 'procedure':
                a.innerHTML = 'Procédure';
                a.href = navbar.lastElementChild.firstChild.href + 'procedure/';
                break;
            case 'procedure-group':
                a.innerHTML = 'Groupe de procédures';
                a.href = navbar.lastElementChild.firstChild.href + 'procedure-group/';
                break;
            case 'service':
                a.innerHTML = 'Tarifs et prestations';
                a.href = navbar.lastElementChild.firstChild.href + 'service/';
                break;
            case 'client':
                a.innerHTML = 'Client';
                a.href = navbar.lastElementChild.firstChild.href + 'client/';
                break;
            case 'account':
                a.innerHTML = 'Compte logiciel';
                a.href = navbar.lastElementChild.firstChild.href + 'account/';
                break;
            case 'finish-status':
                a.innerHTML = 'Prévenir le client';
                a.href = navbar.lastElementChild.firstChild.href + 'finish-status/';
                break;
            case 'finish-fixing':
                a.innerHTML = 'Terminer un dépannage';
                a.href = navbar.lastElementChild.firstChild.href + 'finish-fixing/';
                break;
            case 'add':
                a.innerHTML = 'Ajouter';
                a.href = navbar.lastElementChild.firstChild.href + 'add/';
                break;
            case 'order':
                a.innerHTML = 'Commande';
                a.href = navbar.lastElementChild.firstChild.href + 'order/';
                break;
            case 'edit':
                a.innerHTML = 'Modifier';
                a.href = navbar.lastElementChild.firstChild.href + 'edit/';
                url.splice(key + 1, 1);
                break;
            default:
                a.innerHTML = path;
                a.href = navbar.lastElementChild.firstChild.href + path;
        }
        li.appendChild(a);
        navbar.appendChild(li);
    });
    navbar.lastElementChild.className = navbar.lastElementChild.className + ' text-dark active';
    navbar.lastElementChild.innerHTML = navbar.lastElementChild.firstChild.innerHTML;
}

if (document.querySelector("#js-navbar") != null) {
    createNavbar();
}