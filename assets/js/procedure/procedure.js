function convertToJSONSave() {
    let name = document.getElementById('procedureName').value;
    if (clicked > 0 || name == "")
        return;
    clicked++;
    let data = [];
    let tmp = [];
    let elements = document.querySelectorAll('.element');
    for(let i = 0; i < elements.length; i++) {
        tmp.push(elements[i].firstElementChild.firstElementChild.value);
        tmp.push(elements[i].lastElementChild.lastElementChild.value);
        data.push(tmp);
        if (tmp[1] === "") {
            alert("Certains champs ne sont pas remplis");
            clicked--;
            return;
        }
        tmp = [];
    }
    let json = JSON.stringify({"name" : name, "data" : data});
    let url = location.includes('edit') ? "../update/"+id.toString() : "save";
    let xml = new XMLHttpRequest();
    xml.open("POST", url);
    xml.setRequestHeader("ContentType", "application/json");
    xml.onreadystatechange = function() {
        if(xml.readyState == 4 && xml.status == 200) {
            window.location.pathname= '/backoffice/procedure';
        }
    }
    xml.send(json);
}

function deleteProcedure() {
    let procedureId = this.id;
    if (confirm("Etes vous sur de vouloir supprimer ?")) {
        let xml = new XMLHttpRequest();
        xml.open("GET", "procedure/remove/"+procedureId.substr(6));
        xml.onreadystatechange = function() {
            if(xml.readyState == 4 && xml.status == 200) {
                alert("Procédure supprimée correctement");
                window.location.reload();
            }
        }
        xml.send();
    }
}

function addLine() {
    let tr = document.getElementById('procedureElement').firstElementChild;
    let newTr = tr.cloneNode(true);
    newTr.lastElementChild.lastElementChild.value = "";
    document.getElementById('procedureElement').appendChild(newTr);
}

function removeLine() {
    let procedure = document.getElementById('procedureElement');
    if (procedure.childElementCount > 1)
        procedure.removeChild(procedure.lastElementChild);
}

let location = window.location.pathname;
let id;
let clicked = 0;

import '../navbar';

if (location.includes('edit')) {
    id = document.querySelector('.js-procedure-id').dataset.procedureId;
}

if (!location.includes('add') && !location.includes('edit')) {
    let procedures  = document.querySelectorAll('.deleteProcedure');
    procedures.forEach(procedure => {
        procedure.addEventListener('click', deleteProcedure);
    });
} else {
    let saveButton = document.getElementById("save");
    saveButton.addEventListener('click', convertToJSONSave);

    let button = window.document.getElementById("addLine");
    button.addEventListener('click', addLine);

    let removeButton = document.getElementById("removeLine");
    removeButton.addEventListener('click', removeLine)
}