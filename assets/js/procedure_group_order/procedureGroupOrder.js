function saveOrder() {
    let order = [];
    let procedures = document.querySelectorAll(".procedure");
    procedures.forEach(procedure => {
        order.push(parseInt(procedure.firstElementChild.nextElementSibling.id));
    });
    let json = JSON.stringify({"data" : order});
    let url = "../save-order/"+id.toString();
    let xml = new XMLHttpRequest();
    xml.open("POST", url);
    xml.setRequestHeader("ContentType", "application/json");
    xml.onreadystatechange = function() {
        if(xml.readyState == 4 && xml.status == 200) {
            window.location.pathname= '/backoffice/procedure-group';
        }
    }
    xml.send(json);
}

function up() {
    if (this.parentNode.id.substr(5) == '1')
        return;
    let div = this.parentNode;
    let previousDiv = document.querySelector('#order' + (parseInt(div.id.substr(5)) - 1));
    let parent = document.body;
    parent.insertBefore(div, previousDiv);
    let tmp = this.parentNode.id.substr(5);
    div.id = 'order' + previousDiv.id.substr(5);
    previousDiv.id = 'order' + tmp;
}

function down() {
    if ((parseInt(this.parentNode.id.substr(5))) == max)
        return;
    let div = this.parentNode;
    let nextDiv = document.querySelector('#order' + (parseInt(div.id.substr(5)) + 1));
    let parent = document.body;
    parent.insertBefore(nextDiv, div);
    let tmp = this.parentNode.id.substr(5);
    div.id = 'order' + nextDiv.id.substr(5);
    nextDiv.id = 'order' + tmp;
}

let buttonUp = document.querySelectorAll(".js-button-up");
buttonUp.forEach(button => {
    button.addEventListener('click', up);
});

let buttonDown = document.querySelectorAll(".js-button-down");
buttonDown.forEach(button => {
    button.addEventListener('click', down);
});

let max = buttonDown.length;
let id = document.querySelector('.js-procedure-group-id').dataset.procedureGroupId;
document.getElementById("js-save-order").addEventListener('click', saveOrder);
import '../navbar';