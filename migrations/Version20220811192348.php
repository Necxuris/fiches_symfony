<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220811192348 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fixing ADD fixing_tool_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fixing ADD CONSTRAINT FK_3A6892D64C20F62F FOREIGN KEY (fixing_tool_id) REFERENCES fixing_tool (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3A6892D64C20F62F ON fixing (fixing_tool_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE fixing DROP FOREIGN KEY FK_3A6892D64C20F62F');
        $this->addSql('DROP INDEX UNIQ_3A6892D64C20F62F ON fixing');
        $this->addSql('ALTER TABLE fixing DROP fixing_tool_id');
    }
}
