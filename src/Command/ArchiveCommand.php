<?php

namespace App\Command;

use App\Entity\Stats;
use App\Repository\ClientRepository;
use App\Repository\FixingRepository;
use App\Repository\RecordsFixingRepository;
use App\Service\ClientService;
use App\Service\FixingService;
use App\Service\FixingToolService;
use App\Service\RecordsFixingService;
use App\Service\StatsService;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;


class ArchiveCommand extends Command
{
    private $fixingRepository;
    private $recordsFixingService;
    private $recordsFixingRepository;
    private $clientService;
    private $fixingService;
    private $statsService;
    private $clientRepository;
    private $fixingToolService;
    private $manager;

    public function __construct(
        FixingRepository $fixingRepository,
        RecordsFixingService $recordsFixingService,
        RecordsFixingRepository $recordsFixingRepository,
        ClientService $clientService,
        ClientRepository $clientRepository,
        FixingService $fixingService,
        StatsService $statsService,
        FixingToolService $fixingToolService,
        EntityManagerInterface $manager
    ) {
        $this->fixingRepository = $fixingRepository;
        $this->clientRepository = $clientRepository;
        $this->recordsFixingService = $recordsFixingService;
        $this->recordsFixingRepository = $recordsFixingRepository;
        $this->clientService = $clientService;
        $this->fixingService = $fixingService;
        $this->statsService = $statsService;
        $this->fixingToolService = $fixingToolService;
        $this->manager = $manager;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('archive')
            ->setDescription(
                'Archive les dépannages de plus d\'un mois, supprime les données des clients ayant refusé le RGPD 
                et enregistre une entrée dans la table stats le premier du mois'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new DateTime('now');
        if ($date->format('d') == "01") {
            $dateStats = new DateTime('now');
            $stats = $this->statsService->getStatsFromMonth(
                $dateStats->modify('last day of previous month'),
                $this->clientRepository,
                $this->fixingRepository,
                $this->recordsFixingRepository
            );
            $stats->setDate($dateStats);
            $this->statsService->persistStats($stats);
        }
        // $datetmp = new DateTime("2022-09-30 9:00:00");
        // for ($i = 0; $i < 11; $i++) {
        //     $stats = new Stats();
        //     $stats->setClients($this->clientRepository->getNumberClientFromMonth($datetmp)[0][1]);
        //     $fixings = $this->fixingRepository->getFixingFromMonth($datetmp);
        //     $recordsFixings = $this->recordsFixingRepository->getRecordsFixingsFromMonth($datetmp);
        //     $stats->setFixings(count($fixings) + count($recordsFixings));
    
        //     $labor = 0;
        //     $total = 0;
    
        //     foreach ($fixings as $fixing) {
        //         foreach ($fixing->getServices() as $service) {
        //             if ($service->getType() == 'P' || $service->getType() == 'F') {
        //                 $labor += $service->getPrice();
        //             }
        //             $total += $service->getPrice();
        //         }
        //         foreach ($fixing->getOrders() as $order) {
        //             $total += $order->getPrice();
        //         }
        //     }

        //     foreach ($recordsFixings as $recordsFixing) {
        //         foreach ($recordsFixing->getRecordsServices() as $service) {
        //             if ($service->getType() == 'P' || $service->getType() == 'F') {
        //                 $labor += $service->getPrice();
        //             }
        //             $total += $service->getPrice();
        //         }
        //     }
    
        //     $stats->setLabor($labor);
        //     $stats->setTotal($total);
        //     $stats->setDate($datetmp);
        //     $this->statsService->persistStats($stats);
        //     dump($datetmp);
        //     $datetmp->modify('last day of next month');
        // }

        $warantlyFixings = $this->fixingRepository->getByWarantly(true);

        foreach ($warantlyFixings as $fixing) {
            if ($date->diff($fixing->getEndDateTime())->format("%a") > 30) {
                $output->writeln($fixing->getId());
                $this->recordsFixingService->persistRecordsFixing(
                    $fixing,
                    $fixing->getComputer()->getClient()->getAllowDataStorage()
                );
                $output->writeln("record effectué");
                foreach ($fixing->getActions() as $action) {

                    $fixing->removeAction($action);
                }
                $this->manager->flush();
                
                $output->writeln("Supression des actions");
                foreach ($fixing->getOrders() as $order) {
                    $fixing->removeOrder($order);
                }
                $this->manager->flush();

                $output->writeln("Supression des commandes");
                if ($fixing->getFixingtool() != null) {
                    $this->fixingToolService->removeFixingTool($fixing->getFixingtool());
                    $fixing->setFixingTool(null);
                    $this->manager->flush();
                }

                $output->writeln("Supression des outils");
                $client = $fixing->getComputer()->getClient();

                $this->fixingService->removeFixing($fixing);
                $this->manager->flush();

                if ($client->getAllowDataStorage() == false) {
                    $this->clientService->removeClient($client);
                }
                $this->manager->flush();

                $output->writeln("suppression effectué");
            }
        }
        return Command::SUCCESS;
    }
}
