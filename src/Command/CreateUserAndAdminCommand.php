<?php

namespace App\Command;

use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use App\Service\EmployeeService;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserAndAdminCommand extends Command
{
    private $employeeService;
    private $employeeRepository;
    private $passwordHasher;

    public function __construct(
        EmployeeService $employeeService,
        EmployeeRepository $employeeRepository,
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->employeeService = $employeeService;
        $this->employeeRepository = $employeeRepository;
        $this->passwordHasher = $passwordHasher;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('create_admin')
            ->setDescription(
                'Archive les dépannages de plus d\'un mois et supprime le données des clients ayant refusé le RGPD'
            )
            ->setHelp('Cette commande permet de créer le premier admin et l\'utilisateur de prise en charge.')
            ->addArgument('email', InputArgument::REQUIRED, 'L\'email de l\'admin')
            ->addArgument('password', InputArgument::REQUIRED, 'Le mot de passe de l\'admin')
            ->addArgument('lastname', InputArgument::REQUIRED, 'Le nom de l\'admin')
            ->addArgument('firstname', InputArgument::REQUIRED, 'Le prénom de l\'admin')
            ->addArgument('user_password', InputArgument::REQUIRED, 'Le mot de passe de l\'utilisateur')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($this->employeeRepository->findAll() != null) {
            $output->writeln('Erreur : Il existe déjà des salariés dans la base');
            return Command::INVALID;
        }
        $admin = new Employee();
        $user = new Employee();

        $admin->setEmail($input->getArgument('email'))
              ->setPassword($input->getArgument('password'))
              ->setLastname($input->getArgument('lastname'))
              ->setFirstname($input->getArgument('firstname'))
              ->setRoles(['ROLE_ADMIN']);
        $user->setEmail('pick_up_user@microfun.fr')
             ->setPassword($input->getArgument('user_password'))
             ->setLastname('pick_up')
             ->setFirstname('user')
             ->setRoles(['ROLE_USER']);

        $this->employeeService->persistEmployee($admin, $this->passwordHasher);
        $this->employeeService->persistEmployee($user, $this->passwordHasher);

        return Command::SUCCESS;
    }
}
