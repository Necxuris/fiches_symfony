<?php

namespace App\Command;

use App\Entity\Crowd;
use App\Repository\FixingRepository;
use App\Service\CrowdService;
use DateTime;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CrowdCommand extends Command
{
    private $fixingRepository;
    private $crowdService;

    public function __construct(
        FixingRepository $fixingRepository,
        CrowdService $crowdService
    ) {
        $this->fixingRepository = $fixingRepository;
        $this->crowdService = $crowdService;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('crowd')
            ->setDescription(
                'Enregistre le numbre de dépannges pris en charge chaque jour'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $date = new DateTime('yesterday');
        $crowd = new Crowd();
        $crowd->setNumber($this->fixingRepository->countFixingByDay($date)[0][1]);
        $crowd->setDate($date);
        $this->crowdService->persistCrowd(($crowd));
        $output->writeln("commande affluence");
        return Command::SUCCESS;
    }
}
