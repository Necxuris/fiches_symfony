<?php

namespace App\Controller;

use App\Repository\AddressRepository;
use App\Repository\CityRepository;
use App\Repository\PostcodeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends AbstractController
{
    #[Route('/postcode', name: 'postcode')]
    public function index(
        Request $request,
        PostcodeRepository $postcodeRepository
    ): Response {
        $json = json_decode($request->getContent());
        $postcodes = $postcodeRepository->getPostcode($json->data);
        $data = [];
        foreach ($postcodes as $postcode) {
            foreach ($postcode->getCities() as $city) {
                array_push(
                    $data,
                    $postcode->getPostcode() . ', ' . $city->getName()
                );
            }
        }
        return $this->json(['code' => 200, 'data' => $data], 200);
    }

    #[Route('/address', name: 'address')]
    public function searchAddress(
        Request $request,
        AddressRepository $addressRepository,
        CityRepository $cityRepository
    ): Response {
        $json = json_decode($request->getContent());
        $data = [];
        if ($json->city == "") {
            $addresses = $addressRepository->getAddressWithoutCity($json->data);
        } else {
            $addresses = $addressRepository->getAddressWithCity(
                $json->data,
                $cityRepository->findOneBy(['name' => $json->city])->getId()
            );
        }
        foreach ($addresses as $address) {
            array_push(
                $data,
                $address->getName()
                . ', ' . $address->getCity()->getName()
                . ', ' . $address->getCity()->getPostcode()->getPostcode()
            );
        }
        return $this->json(['code' => 200, 'data' => $data], 200);
    }
}
