<?php

namespace App\Controller;

use App\Repository\AccountRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeAccountController extends AbstractController
{
    /**
     * @Route("/backoffice/account", name="backoffice_account")
     */
    public function index(AccountRepository $accountRepository): Response
    {
        return $this->render('backoffice/account/index.html.twig', [
            'navbar' => 'BackOffice compte',
            'employee' => $this->getUser(),
            'accounts' => $accountRepository->findAll(),

        ]);
    }
}
