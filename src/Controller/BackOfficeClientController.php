<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use App\Repository\FixingRepository;
use App\Service\ClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeClientController extends AbstractController
{
    /**
     * @Route("/backoffice/client", name="backoffice_client")
     */
    public function index(ClientRepository $clientRepository): Response
    {
        return $this->render('backoffice/client/index.html.twig', [
            'navbar' => 'BackOffice client',
            'clients' => $clientRepository->findAll(),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/client/edit/{id}", name="backoffice_client_edit")
     */
    public function editClient(
        Request $request,
        Client $client,
        ClientService $clientService
    ): Response {
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $clientService->persistClient($client);

            return $this->redirectToRoute('backoffice_client');
        }
        return $this->renderForm('backoffice/client/edit/index.html.twig', [
            'navbar' => 'BackOffice client',
            'form' => $form,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/client/remove/{id}", name="backoffice_client_remove")
     */
    public function removeClient(
        Client $client,
        ClientService $clientService,
        FixingRepository $fixingRepository
    ): Response {
        foreach ($client->getComputers() as $computer) {
            if ($fixingRepository->getByComputer($computer->getId()) != null) {
                return $this->json(
                    ['code' => 500, 'message' => 'Client non supprimé, dépannage en cours sur un PC'],
                    405
                );
            }
        }
        $clientService->removeClient($client);
        return $this->json(['code' => 200, 'message' => 'Client deleted'], 200);
    }
}
