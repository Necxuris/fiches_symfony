<?php

namespace App\Controller;

use App\Entity\Stats;
use App\Repository\ClientRepository;
use App\Repository\FixingRepository;
use App\Repository\RecordsFixingRepository;
use App\Repository\StatsRepository;
use App\Service\AlertService;
use App\Service\ChartService;
use App\Service\StatsService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeController extends AbstractController
{
    /**
     * @Route("/backoffice", name="app_backoffice")
     */
    public function index(
        StatsRepository $statsRepository,
        StatsService $statsService,
        ClientRepository $clientRepository,
        FixingRepository $fixingRepository,
        ChartService $chartService,
        RecordsFixingRepository $recordsFixingRepository
    ): Response {
        $date = new DateTime('now');
        $statsMonth = $statsService->getStatsFromMonth(
            $date,
            $clientRepository,
            $fixingRepository,
            $recordsFixingRepository
        );
        $date->modify('last day of previous month');
        if ($statsRepository->getStatsFromMonth($date) != null) {
            $previousMonth = $statsRepository->getStatsFromMonth($date)[0];
        } else {
            $previousMonth = new Stats();
        }
        $stats = $statsService->statsBackoffice($statsMonth, $previousMonth);
        $date = new DateTime('now');

        $charts = $chartService->createChartCart($date);

        $firstDate = new DateTime();
        $firstDate = $firstDate->createFromFormat('m/Y', '07/2022');
        $firstYear = $firstDate->format('Y');
        $dateYear = $date->format('Y');
        $firstMonth = $firstDate->format('m');
        $dateMonth = $date->format('m');
        $diff = (($dateYear - $firstYear) * 12) + ($dateMonth - $firstMonth);
        $listDates = [];
        for ($i = 0; $i <= $diff; $i++) {
            array_push($listDates, $firstDate->format('Y-m'));
            $firstDate->modify('last day of next month');
        }

        return $this->render('backoffice/index.html.twig', [
            'navbar' => 'BackOffice',
            'employee' => $this->getUser(),
            'stats' => $stats,
            'listdates' => $listDates,
            'jsonMoMoy' => $charts['jsonMoMoy'],
            'jsonMoy' => $charts['jsonMoy'],
            'jsonMo' => $charts['jsonMo'],
            'jsonMat' => $charts['jsonMat'],
            'jsonCrowd' => $charts['jsonCrowd'],
            'jsonTop' => $chartService->getTopServices($fixingRepository->getByWarantly(true))
        ]);
    }

    /**
     * @Route("/backoffice/testSMS", name="backoffice_test_sms")
     */
    public function testSMS(AlertService $alertService, Request $request): Response
    {
        $json = json_decode($request->getContent());
        $alertService->alertMQTT($json->phone, 'backoffice');
        return $this->json(['code' => 200, 'message' => 'message envoyé'], 200);
    }

    /**
     * @Route("/backoffice/change-stats", name="backoffice_change_stats")
     */
    public function changeStats(
        ChartService $chartService,
        StatsService $statsService,
        StatsRepository $statsRepository,
        ClientRepository $clientRepository,
        FixingRepository $fixingRepository,
        RecordsFixingRepository $recordsFixingRepository,
        Request $request
    ): Response {
        $json = json_decode($request->getContent());
        $date = new DateTime();
        $date = $date->createFromFormat('Y-m', $json->date);

        $actualDate = new DateTime('now');
        if ($date->format('Y-m') == $actualDate->format('Y-m')) {
            $topServices = $chartService->getTopServices($fixingRepository->getByWarantly(true));
        } else {
            $topServices = $chartService->getTopRecordsServices(
                $recordsFixingRepository->getRecordsFixingsFromMonth($date)
            );
        }
        $charts = $chartService->createChartCart($date);

        $statsMonth = $statsService->getStatsFromMonth(
            $date,
            $clientRepository,
            $fixingRepository,
            $recordsFixingRepository
        );
        $date->modify('last day of previous month');
        if ($statsRepository->getStatsFromMonth($date) != null) {
            $previousMonth = $statsRepository->getStatsFromMonth($date)[0];
        } else {
            $previousMonth = new Stats();
        }
        $stats = $statsService->statsBackoffice($statsMonth, $previousMonth);
        $returnJSON = array(
            'charts' => $charts,
            'stats' => $stats,
            'topServices' => $topServices
        );
        return $this->json($returnJSON, 200);
    }
}
