<?php

namespace App\Controller;

use DateTime;
use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Repository\EmployeeRepository;
use App\Repository\FixingRepository;
use App\Service\EmployeeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeEmployeeController extends AbstractController
{
    /**
     * @Route("/backoffice/employee", name="backoffice_employee")
     */
    public function index(EmployeeRepository $employeeRepository, FixingRepository $fixingRepository): Response
    {
        // $this->denyAccessUnlessGranted('ROLE_ADMIN');

        return $this->render('backoffice/employee/index.html.twig', [
            'navbar' => 'BackOffice salarié',
            'employees' => $employeeRepository->findAll(),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/employee/add", name="backoffice_employee_add")
     */
    public function addEmployee(
        Request $request,
        EmployeeService $employeeService,
        UserPasswordHasherInterface $passwordHasher
    ): Response {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $employee = $form->getData();
            $employeeService->persistEmployee($employee, $passwordHasher);

            return $this->redirectToRoute('backoffice_employee');
        }

        return $this->renderForm('backoffice/employee/add/index.html.twig', [
            'navbar' => 'BackOffice salarié',
            'form' => $form,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/employee/edit/{id}", name="backoffice_employee_edit")
     */
    public function editEmployee(
        Request $request,
        Employee $employee,
        EmployeeService $employeeService,
        UserPasswordHasherInterface $passwordHasher
    ): Response {
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $employee = $form->getData();
            $employeeService->persistEmployee($employee, $passwordHasher);

            return $this->redirectToRoute('backoffice_employee');
        }

        return $this->renderForm('backoffice/employee/edit/index.html.twig', [
            'navbar' => 'BackOffice salarié',
            'form' => $form,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/employee/remove/{id}", name="backoffice_employee_remove")
     */
    public function removeEmployee(Employee $employee, EmployeeService $employeeService): Response
    {
        $employeeService->removeEmployee($employee);
        return $this->json(['code' => 200, 'message' => 'employee deleted'], 200);
    }
}
