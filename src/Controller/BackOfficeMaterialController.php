<?php

namespace App\Controller;

use App\Entity\Material;
use App\Form\MaterialType;
use App\Repository\MaterialRepository;
use App\Service\MaterialService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeMaterialController extends AbstractController
{
    /**
     * @Route("/backoffice/material", name="backoffice_material")
     */
    public function index(MaterialRepository $materialRepository): Response
    {
        return $this->render('backoffice/material/index.html.twig', [
            'navbar' => 'BackOffice matériel déposé',
            'materials' => $materialRepository->findAll(),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/material/add", name="backoffice_material_add")
     */
    public function addMaterial(Request $request, MaterialService $materialService): Response
    {
        $material = new Material();
        $form = $this->createForm(MaterialType::class, $material);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $material = $form->getData();
            $materialService->persistmaterial($material);

            return $this->redirectToRoute('backoffice_material');
        }
        return $this->renderForm('backoffice/material/add/index.html.twig', [
            'navbar' => 'BackOffice matériel déposé',
            'form' => $form,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/material/edit/{id}", name="backoffice_material_edit")
     */
    public function editMaterial(
        Request $request,
        Material $material,
        MaterialService $materialService
    ): Response {
        $form = $this->createForm(MaterialType::class, $material);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $material = $form->getData();
            $materialService->persistmaterial($material);

            return $this->redirectToRoute('backoffice_material');
        }
        return $this->renderForm('backoffice/material/edit/index.html.twig', [
            'navbar' => 'BackOffice matériel déposé',
            'form' => $form,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/material/remove/{id}", name="backoffice_material_remove")
     */
    public function removeMaterial(Material $material, MaterialService $materialService): Response
    {
        $materialService->removeMaterial($material);
        return $this->json(['code' => 200, 'message' => 'material deleted'], 200);
    }
}
