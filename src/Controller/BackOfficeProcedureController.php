<?php

namespace App\Controller;

use App\Entity\Procedure;
use App\Repository\ProcedureRepository;
use App\Service\ProcedureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeProcedureController extends AbstractController
{
    /**
     * @Route("/backoffice/procedure", name="backoffice_procedure")
     */
    public function index(ProcedureRepository $procedureRepository): Response
    {
        return $this->render('backoffice/procedure/index.html.twig', [
            'navbar' => 'BackOffice procédure',
            'procedures' => $procedureRepository->findAll(),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/procedure/add", name="backoffice_procedure_add")
     */
    public function addProcedure(): Response
    {
        return $this->render('backoffice/procedure/add/index.html.twig', [
            'navbar' => 'BackOffice procédure',
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/procedure/edit/{id}", name="backoffice_procedure_edit")
     */
    public function editProcedure(Procedure $procedure): Response
    {
        return $this->render('backoffice/procedure/edit/index.html.twig', [
            'navbar' => 'BackOffice procédure',
            'employee' => $this->getUser(),
            'procedure' => $procedure,
        ]);
    }

    /**
     * @Route("/backoffice/procedure/remove/{id}", name="backoffice_procedure_remove")
     */
    public function removeProcedure(Procedure $procedure, ProcedureService $procedureService): Response
    {
        $procedureService->removeProcedure($procedure);
        return $this->json(['code' => 200, 'message' => 'procedure deleted'], 200);
    }

    /**
     * @Route("backoffice/procedure/save", name="backoffice_procedure_save")
     */
    public function saveProcedure(Request $request, ProcedureService $procedureService): Response
    {
        $json = json_decode($request->getContent());
        $procedureService->persistProcedure($json);
        return $this->json(['code' => 200, 'message' => 'procedure saved'], 200);
    }

    /**
     * @Route("backoffice/procedure/update/{id}", name="backoffice_procedure_update")
     */
    public function updateProcedure(
        Request $request,
        ProcedureService $procedureService,
        Procedure $procedure
    ): Response {
        $json = json_decode($request->getContent());
        $procedureService->updateProcedure($json, $procedure);
        return $this->json(['code' => 200, 'message' => 'procedure edited'], 200);
    }
}
