<?php

namespace App\Controller;

use App\Entity\ProcedureGroup;
use App\Form\ProcedureGroupType;
use App\Repository\ProcedureRepository;
use App\Repository\ProcedureGroupRepository;
use App\Service\ProcedureGroupService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeProcedureGroupController extends AbstractController
{
    /**
     * @Route("/backoffice/procedure-group", name="backoffice_procedure_group")
     */
    public function index(ProcedureGroupRepository $procedureGroupRepository): Response
    {
        return $this->render('backoffice/procedure-group/index.html.twig', [
            'navbar' => 'BackOffice groupe de procédures',
            'procedure_groups' => $procedureGroupRepository->findAll(),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/procedure-group/add", name="backoffice_procedure_group_add")
     */
    public function addProcedureGroup(Request $request, ProcedureGroupService $procedureGroupService): Response
    {
        $procedureGroup = new ProcedureGroup();
        $form = $this->createForm(ProcedureGroupType::class, $procedureGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $procedureGroup = $form->getData();
            $procedureGroupService->persistProcedureGroup($procedureGroup);

            return $this->redirectToRoute(
                'backoffice_procedure_group_procedure_order',
                array('id' => $procedureGroup->getId())
            );
        }
        return $this->renderForm('backoffice/procedure-group/add/index.html.twig', [
            'navbar' => 'BackOffice groupe de procédures',
            'employee' => $this->getUser(),
            'form' => $form,
        ]);
    }

    /**
     * @Route("/backoffice/procedure-group/edit/{id}", name="backoffice_procedure_group_edit")
     */
    public function editProcedureGroup(
        Request $request,
        ProcedureGroup $procedureGroup,
        ProcedureGroupService $procedureGroupService
    ): Response {
        $id = $procedureGroup->getId();
        $form = $this->createForm(ProcedureGroupType::class, $procedureGroup);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $procedureGroup = $form->getData();
            $procedureGroupService->persistProcedureGroup($procedureGroup);

            return $this->redirectToRoute('backoffice_procedure_group_procedure_order', array('id' => $id));
        }
        return $this->renderForm('backoffice/procedure-group/edit/index.html.twig', [
            'navbar' => 'BackOffice groupe de procédures',
            'employee' => $this->getUser(),
            'form' => $form,
        ]);
    }

    /**
     * @Route("/backoffice/procedure-group/procedure-order/{id}", name="backoffice_procedure_group_procedure_order")
     */
    public function editProcedureGroupOrder(
        Request $request,
        ProcedureGroup $procedureGroup,
        ProcedureRepository $procedureRepository
    ): Response {
        return $this->render('backoffice/procedure-group/procedure-order/index.html.twig', [
            'navbar' => 'BackOffice groupe de procédures',
            'employee' => $this->getUser(),
            'procedures' => $procedureRepository->getProceduresByOrder($procedureGroup->getId()),
            'id' => $procedureGroup->getId(),
        ]);
    }

    /**
     * @Route("/backoffice/procedure-group/save-order/{id}", name="backoffice_procedure_group_save_order")
     */
    public function saveProcedureGroupOrder(
        Request $request,
        ProcedureGroup $procedureGroup,
        ProcedureGroupService $procedureGroupService
    ): Response {
        $json = json_decode($request->getContent());
        $procedureGroupService->saveOrder($procedureGroup, $json->data);
        return $this->json(['code' => 200, 'message' => 'order saved'], 200);
    }

    /**
     * @Route("/backoffice/procedure-group/remove/{id}", name="backoffice_procedure_group_remove")
     */
    public function removeProcedureGroup(
        ProcedureGroup $procedureGroup,
        ProcedureGroupService $procedureGroupService
    ): Response {
        $procedureGroupService->removeProcedureGroup($procedureGroup);
        return $this->redirectToRoute('backoffice_procedure_group');
    }
}
