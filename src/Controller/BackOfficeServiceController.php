<?php

namespace App\Controller;

use App\Entity\Service;
use App\Form\ServiceType;
use App\Repository\ServiceRepository;
use App\Service\ServiceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BackOfficeServiceController extends AbstractController
{
    /**
     * @Route("/backoffice/service", name="backoffice_service")
     */
    public function index(ServiceRepository $serviceRepository): Response
    {
        return $this->render('backoffice/service/index.html.twig', [
            'navbar' => 'BackOffice tarifs et prestations',
            'prise_en_charges' => $serviceRepository->getServiceByType('P'),
            'forfaits' => $serviceRepository->getServiceByType('F'),
            'materials' => $serviceRepository->getServiceByType('M'),
            'logiciels' => $serviceRepository->getServiceByType('L'),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/backoffice/service/add", name="backoffice_service_add")
     */
    public function addService(Request $request, ServiceService $serviceService): Response
    {
        $service = new Service();
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service = $form->getData();
            $serviceService->persistService($service);

            return $this->redirectToRoute('backoffice_service');
        }
        return $this->renderForm('backoffice/service/add/index.html.twig', [
            'navbar' => 'BackOffice tarifs et prestations',
            'employee' => $this->getUser(),
            'form' => $form,
        ]);
    }

    /**
     * @Route("/backoffice/service/edit/{id}", name="backoffice_service_edit")
     */
    public function editService(
        Request $request,
        Service $service,
        ServiceService $serviceService
    ): Response {
        $form = $this->createForm(ServiceType::class, $service);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service = $form->getData();
            $serviceService->persistService($service);

            return $this->redirectToRoute('backoffice_service');
        }
        return $this->renderForm('backoffice/service/edit/index.html.twig', [
            'navbar' => 'BackOffice tarfis et prestations',
            'employee' => $this->getUser(),
            'form' => $form,
        ]);
    }

    /**
     * @Route("/backoffice/service/remove/{id}", name="backoffice_service_remove")
     */
    public function removeService(Service $service, ServiceService $serviceService): Response
    {
        $serviceService->removeService($service);
        return $this->json(['code' => 200, 'message' => 'service deleted'], 200);
    }
}
