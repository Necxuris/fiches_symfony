<?php

namespace App\Controller;

use App\Repository\FixingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DisplayController extends AbstractController
{
    /**
     * @Route("/display", name="app_display")
     */
    public function index(FixingRepository $fixingRepository): Response
    {
        return $this->render('display/index.html.twig', [
            'controller_name' => 'DisplayController',
        ]);
    }
}
