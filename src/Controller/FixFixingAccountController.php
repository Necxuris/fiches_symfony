<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Fixing;
use App\Form\AccountType;
use App\Service\AccountService;
use App\Service\MercureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

class FixFixingAccountController extends AbstractController
{
    /**
     * @Route("/fix/fixing/{id}/account/add", name="fix_fixing_account_add")
     */
    public function addAccount(
        Fixing $fixing,
        Request $request,
        AccountService $accountService
    ): Response {
        $account = new Account();
        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $account = $form->getData();
            $accountService->persistAccount($account, $this->getUser(), $fixing->getComputer());

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }

        return $this->renderForm('pick-up/account/add/index.html.twig', [
            'form' => $form,
            'employee' => $this->getUser(),
            'navbar' => 'Dépannage en atelier compte',
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/account/edit/{accountId}", name="fix_fixing_account_edit")
     * @Entity("account", expr="repository.find(accountId)")
     */
    public function editAccount(
        Fixing $fixing,
        Request $request,
        Account $account,
        AccountService $accountService
    ): Response {
        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accountService->updateAccount($account, $this->getUser(), $fixing->getComputer());

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }

        return $this->renderForm('pick-up/account/edit/index.html.twig', [
            'form' => $form,
            'employee' => $this->getUser(),
            'navbar' => 'Dépannage en atelier compte',
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/account/remove/{accountId}", name="fix_fixing_account_remove")
     * @Entity("account", expr="repository.find(accountId)")
     */
    public function removeAccount(
        Fixing $fixing,
        Request $request,
        Account $account,
        AccountService $accountService
    ): Response {
        $accountService->removeAccount($account);
        return $this->json(['code' => 200, 'message' => 'Compte supprimé'], 200);
    }
}
