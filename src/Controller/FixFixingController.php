<?php

namespace App\Controller;

use App\Entity\Fixing;
use App\Repository\FixingRepository;
use App\Service\FixingService;
use App\Service\RecordsFixingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FixFixingController extends AbstractController
{
    /**
     * @Route("/fix", name="fix")
     */
    public function index(FixingRepository $fixingRepository): Response
    {
        return $this->render('fix/index.html.twig', [
            'navbar' => 'Dépannage en atelier',
            'fixings' => $fixingRepository->getByWarantly(false),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/finish-fixing", name="fix_finish_fixing")
     */
    public function finishFixing(
        FixingRepository $fixingRepository,
        Request $request,
        FixingService $fixingService
    ): Response {
        $form = $request->request->all();
        $selectedFixing = null;

        if ($form != null && !isset($form['selectedFixing'])) {
            if (!isset($form['fixing'])) {
                return $this->render('fix/finish_fixing/index.html.twig', [
                    'controller_name' => 'FixFinishFixingController',
                    'fixings' => $fixingRepository->getByStatus('Fini'),
                    'selected' => $selectedFixing,
                ]);
            }
            $fixing = $fixingRepository->findOneBy(['id' => $form['fixing']]);
            $fixingService->fixingToWarantly($fixing, $this->getUser(), $form);
        }
        if (isset($form['selectedFixing'])) {
            $selectedFixing = intval($form['selectedFixing']);
        }
        return $this->render('fix/finish_fixing/index.html.twig', [
            'navbar' => 'Dépannage en atelier terminé',
            'fixings' => $fixingRepository->getByStatus('Fini'),
            'selected' => $selectedFixing,
            'employee' => $this->getUser(),
        ]);
    }
}
