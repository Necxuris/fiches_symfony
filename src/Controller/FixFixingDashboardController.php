<?php

namespace App\Controller;

use App\Entity\Fixing;
use App\Entity\FixingTool;
use App\Form\ClientType;
use App\Form\ComputerType;
use App\Form\FixingMaterialType;
use App\Repository\RecordsFixingRepository;
use App\Service\ClientService;
use App\Service\ComputerService;
use App\Service\FixingService;
use App\Service\MaterialService;
use App\Service\MercureService;
use App\Service\PrintService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class FixFixingDashboardController extends AbstractController
{
    /**
     * @Route("/fix/fixing/{id}", name="fix_fixing")
     */
    public function index(Fixing $fixing, RecordsFixingRepository $recordsFixingRepository): Response
    {
        return $this->render('fix/fixing/index.html.twig', [
            'navbar' => 'Dépannage en atelier dashboard',
            'fixing' => $fixing,
            'historyNumber' => count($recordsFixingRepository->findBy(
                ['computer' => $fixing->getComputer()]
            )),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/wait-status", name="fix_fixing_wait_status")
     */
    public function changeStatusWait(
        Fixing $fixing,
        Request $request,
        FixingService $fixingService,
        MercureService $mercureService
    ): Response {
        $form = $request->request->all();
        if ($form != null) {
            $fixingService->waitFixing($fixing, $this->getUser(), $form);
            $mercureService->sendUpdateStatusFixing($fixing, $this->getUser());
            return $this->redirectToRoute('fix_fixing', ['id' => $fixing->getId()]);
        }
        return $this->render('fix/fixing/wait/index.html.twig', [
            'navbar' => 'Dépannage en atelier mise en attente',
            'fixing' => $fixing,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/finish-status", name="fix_fixing_finish_status")
     */
    public function changeStatusFinish(
        Fixing $fixing,
        //MailerInterface $mailer,
        Request $request,
        FixingService $fixingService,
        MercureService $mercureService
    ): Response {
        $form = $request->request->all();
        if ($form != null) {
            $fixingService->finishFixing($fixing, $this->getUser(), $form);
            $mercureService->sendRemoveFixing($fixing);
        }
        return $this->render('fix/fixing/finish/index.html.twig', [
            'navbar' => 'Dépannage en atelier prévenir le client',
            'fixing' => $fixing,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/fixing-tool", name="fix_fixing_fixing_tool")
     */
    public function fixingTool(
        Fixing $fixing,
    ): Response {
        if ($fixing->getFixingTool() == null) {
            $fixing->setFixingTool(new FixingTool());
        }
        return $this->render('fix/fixing/fixing_tool/index.html.twig', [
            'navbar' => 'Dépannage en atelier outils de dépannage',
            'fixing' => $fixing,
            'tool' => ($fixing->getFixingTool()->getData() == [])
                ? json_encode($fixing->getFixingTool()->getData())
                : json_encode($fixing->getFixingTool()->getData()[0]),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/comment", name="fix_fixing_comment")
     */
    public function addComment(
        Fixing $fixing,
        Request $request,
        FixingService $fixingService,
        MercureService $mercureService
    ): Response {
        $json = json_decode($request->getContent());
        $fixingService->addComment($fixing, $this->getUser(), $json);
        $mercureService->sendUpdateTechFixing($fixing, $this->getUser());
        return $this->json(['code' => 200, 'message' => 'Commentaire sauvegardé avec succès'], 200);
    }

    /**
     * @Route("/fix/fixing/{id}/history", name="fix_fixing_history")
     */
    public function history(
        Fixing $fixing,
        RecordsFixingRepository $recordsFixingRepository
    ): Response {
        return $this->render('fix/fixing/history/index.html.twig', [
            'navbar' => 'Dépannage en atelier historique',
            'recordsFixings' => $recordsFixingRepository->findBy(
                ['computer' => $fixing->getComputer()]
            ),
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/renew-display", name="fix_renew_display")
     */
    public function renewDisplay(
        Fixing $fixing,
        MercureService $mercureService
    ): Response {
        $mercureService->sendNewFixing($fixing);
        return $this->json(['code' => 200, 'message' => 'Affichage refait'], 200);
    }

    /**
     * @Route("/fix/fixing/{id}/print-ticket", name="fix_print_ticket")
     */
    public function printTicket(
        Fixing $fixing,
        PrintService $printService
    ): Response {
        $printService->removeTicket($fixing);
        return $this->json(['code' => 200, 'message' => 'ticket imprimé'], 200);
    }

    /**
     * @Route("/fix/fixing/{id}/fixing-tool/save", name="fix_fixing_tool_save")
     */
    public function saveFixingTool(
        Fixing $fixing,
        Request $request,
        FixingService $fixingService
    ): Response {
        $date = new DateTime('now');
        $json = json_decode($request->getContent());
        if ($fixing->getFixingTool() == null) {
            $fixingTool = new FixingTool();
            $fixing->setFixingTool($fixingTool);
        }
        $fixing->getFixingTool()->setData($json);
        $fixing->setComment(
            $fixing->getComment()
            . '¤Action§'
            . $this->getUser()->getInitials()
            . ' - Modifications des outils de dépannages§'
            . $date->format('d-m-Y H:i:s')
        );
        $fixingService->updateFixing($fixing);
        return $this->json(['code' => 200, 'message' => 'toolbox bien enregistré'], 200);
    }

    /**
     * @Route("/fix/fixing/{id}/client", name="fix_fixing_client")
     */
    public function editClient(
        Fixing $fixing,
        Request $request,
        ClientService $clientService
    ): Response {
        $form = $this->createForm(ClientType::class, $fixing->getComputer()->getClient());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clientService->updateClient();

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }

        return $this->renderForm('pick-up/client/add/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/computer", name="fix_fixing_computer")
     */
    public function editComputer(
        Fixing $fixing,
        Request $request,
        ComputerService $computerService
    ): Response {
        $form = $this->createForm(ComputerType::class, $fixing->getComputer());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $computerService->updateComputer();

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }

        return $this->renderForm('pick-up/computer/add/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/material", name="fix_fixing_material")
     */
    public function editMaterials(
        Fixing $fixing,
        Request $request,
        MaterialService $materialService
    ): Response {
        $form = $this->createForm(FixingMaterialType::class, $fixing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $materialService->updateMaterial();

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }

        return $this->renderForm('fix/fixing/material/index.html.twig', [
            'form' => $form,
        ]);
    }
}
