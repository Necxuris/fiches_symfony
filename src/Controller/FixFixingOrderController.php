<?php

namespace App\Controller;

use App\Entity\Fixing;
use App\Entity\Order;
use App\Form\OrderType;
use App\Service\MercureService;
use App\Service\OrderService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

class FixFixingOrderController extends AbstractController
{
    /**
     * @Route("/fix/fixing/{id}/order/add", name="fix_fixing_add_order")
     */
    public function addOrder(
        Fixing $fixing,
        Request $request,
        OrderService $orderService,
        MercureService $mercureService
    ): Response {
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);
        $date = new DateTime('now');

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $fixing->setComment(
                $fixing->getComment()
                . '¤Commande§'
                . $this->getUser()->getInitials()
                . ' - '
                . $order->getDescription()
                . '§'
                . $date->format('d-m-Y H:i:s')
            );
            $orderService->persistOrder($order, $date, $fixing, $this->getUser());
            $mercureService->sendUpdateTechFixing($fixing, $this->getUser());

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }
        return $this->renderForm('fix/fixing/order/index.html.twig', [
            'navbar' => 'Dépannage en atelier commande',
            'form' => $form,
            'employee' => $this->getUser()
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/order/edit/{orderId}", name="fix_fixing_edit_order")
     * @Entity("order", expr="repository.find(orderId)")
     */
    public function editOrder(
        Fixing $fixing,
        Order $order,
        Request $request,
        OrderService $orderService
    ): Response {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();
            $orderService->updateOrder();

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }
        return $this->renderForm('fix/fixing/order/index.html.twig', [
            'navbar' => 'Dépannage en atelier commande',
            'form' => $form,
            'employee' => $this->getUser()
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/order/remove/{orderId}", name="fix_fixing_remove_order")
     * @Entity("order", expr="repository.find(orderId)")
     */
    public function removeOrder(
        Fixing $fixing,
        Order $order,
        OrderService $orderService
    ): Response {
        $date = new DateTime('now');
        $fixing->setComment(
            $fixing->getComment()
            . '¤Commande§'
            . $this->getUser()->getInitials()
            . ' - '
            . $order->getDescription()
            . ' - commande supprimée§'
            . $date->format('d-m-Y H:i:s')
        );
        $orderService->removeOrder($order);
        return $this->json(['code' => 200, 'message' => 'order deleted'], 200);
    }
}
