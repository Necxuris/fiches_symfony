<?php

namespace App\Controller;

use App\Entity\Action;
use App\Entity\Fixing;
use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use App\Form\FixingProceduresType;
use App\Repository\ActionRepository;
use App\Repository\ProcedureRepository;
use App\Service\ActionService;
use App\Service\FixingService;
use App\Service\MercureService;
use App\Service\ProcedureGroupService;
use App\Service\ProcedureService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

class FixFixingProcedureController extends AbstractController
{
    /**
     * @Route("/fix/fixing/{id}/procedure", name="fix_fixing_add_edit_procedure")
     */
    public function addProcedureAndGroup(
        Request $request,
        Fixing $fixing,
        FixingService $fixingService,
        MercureService $mercureService
    ): Response {
        $form = $this->createForm(FixingProceduresType::class, $fixing);
        $form->handleRequest($request);
        $date = new DateTime('now');

        if ($form->isSubmitted() && $form->isValid()) {
            $fixing = $form->getData();
            if ($fixing->getStatus() == 'Pris en charge') {
                $fixing->setStatus('En cours');
                $fixing->setComment(
                    $fixing->getComment()
                    . '¤Status§'
                    . $this->getUser()->getInitials()
                    . ' - En cours§'
                    . $date->format('d-m-Y H:i:s')
                );
            }
            $fixing->setComment(
                $fixing->getComment()
                . '¤Action§'
                . $this->getUser()->getInitials()
                . ' - Modification des procédures§'
                . $date->format('d-m-Y H:i:s')
            );
            $fixingService->updateFixing($fixing);
            $mercureService->sendUpdateStatusFixing($fixing, $this->getUser());

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }
        return $this->renderForm('fix/fixing/procedure/add-edit-procedure/index.html.twig', [
            'navbar' => 'Dépannage en atelier groupe de procédures',
            'form' => $form,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/procedure/save-action", name="fix_fixing_save_action")
     */
    public function saveAction(
        Request $request,
        Fixing $fixing,
        ActionService $actionService,
        MercureService $mercureService
    ): Response {
        $json = json_decode($request->getContent());
        $date = new DateTime('now');
        $action = new Action();
        $fixing->setComment(
            $fixing->getComment()
            . '¤Action§'
            . $this->getUser()->getInitials()
            . ' - '
            . $json->name
            . ' : '
            . $json->value
            . '§'
            . $date->format('d-m-Y H:i:s')
        );
        $actionService->persistAction($action, $fixing, $this->getUser(), $json);
        $mercureService->sendUpdateTechFixing($fixing, $this->getUser());
        return $this->json(['code' => 200, 'message' => 'action saved'], 200);
    }

    /**
     * @Route("/fix/fixing/{id}/procedure/procedure={procedureId}", name="fix_fixing_do_procedure")
     * @Entity("procedure", expr="repository.find(procedureId)")
     */
    public function doProcedure(
        Fixing $fixing,
        Procedure $procedure,
        ProcedureService $procedureService,
        ActionRepository $actionRepository,
        ActionService $actionService
    ): Response {
        $elements = $procedureService->getElements($procedure->getElements());
        $actions = $actionService->getUniqueActions($actionRepository, $fixing, $procedure);
        return $this->render('fix/fixing/procedure/do-procedure/index.html.twig', [
            'navbar' => 'Dépannage en atelier effectuer une procédure',
            'procedureId' => $procedure->getId(),
            'name' => $procedure->getName(),
            'elements' => $elements,
            'actions' => $actions,
            'employee' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/fix/fixing/{id}/procedure/procedure-group={procedureGroupId}", name="fix_fixing_do_procedure_group")
     * @Entity("procedureGroup", expr="repository.find(procedureGroupId)")
     */
    public function doProcedureGroup(
        Fixing $fixing,
        ProcedureGroup $procedureGroup,
        ProcedureGroupService $procedureGroupService,
        ProcedureRepository $procedureRepository,
        ActionRepository $actionRepository,
        ActionService $actionService,
        ProcedureService $procedureService
    ): Response {
        $procedureElements = $procedureGroupService->getElementsFromProcedure(
            $procedureRepository->getProceduresByOrder($procedureGroup->getId()),
            $procedureService
        );
        $actions = $actionService->getUniqueActionsGroup($actionRepository, $fixing, $procedureGroup);
        return $this->render('fix/fixing/procedure/do-procedure-group/index.html.twig', [
            'navbar' => 'Dépannage en atelier effectuer un groupe de procédure',
            'procedureGroupId' => $procedureGroup->getId(),
            'procedureGroupName' => $procedureGroup->getName(),
            'procedureElements' => $procedureElements,
            'procedures' => $procedureRepository->getProceduresByOrder($procedureGroup->getId()),
            'actions' => $actions,
            'employee' => $this->getUser(),
        ]);
    }
}
