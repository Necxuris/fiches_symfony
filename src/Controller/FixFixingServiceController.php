<?php

namespace App\Controller;

use App\Entity\Fixing;
use App\Entity\Service;
use App\Form\FixingServicesType;
use App\Service\FixingService;
use App\Service\MercureService;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FixFixingServiceController extends AbstractController
{
    /**
     * @Route("/fix/fixing/{id}/service", name="fix_fixing_service")
     */
    public function editService(
        Fixing $fixing,
        Request $request,
        FixingService $fixingService,
        MercureService $mercureService
    ): Response {
        $form = $this->createForm(FixingServicesType::class, $fixing);
        $form->handleRequest($request);
        $date = new DateTime('now');

        if ($form->isSubmitted() && $form->isValid()) {
            $fixing = $form->getData();
            $fixing->setComment(
                $fixing->getComment()
                . '¤Action§'
                . $this->getUser()->getInitials()
                . ' - Modification des tarifs et prestations§'
                . $date->format('d-m-Y H:i:s')
            );
            $fixingService->updateFixing();
            $mercureService->sendUpdateTechFixing($fixing, $this->getUser());

            return $this->redirectToRoute('fix_fixing', array('id' => $fixing->getId()));
        }
        return $this->renderForm('fix/fixing/service/index.html.twig', [
            'navbar' => 'Dépannage en atelier modifier les tarifs et prestations',
            'form' => $form,
            'employee' => $this->getUser()
        ]);
    }
}
