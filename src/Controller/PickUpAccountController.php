<?php

namespace App\Controller;

use App\Entity\Account;
use App\Entity\Client;
use App\Entity\Computer;
use App\Entity\Employee;
use App\Form\AccountType;
use App\Service\AccountService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\Request;

class PickUpAccountController extends AbstractController
{
    /**
     * @Route("/pick-up/{id}/{clientId}/{computerId}/account/add", name="pick_up_account_add")
     * @Entity("client", expr="repository.find(clientId)")
     * @Entity("computer", expr="repository.find(computerId)")
     */
    public function addAccount(
        Employee $employee,
        Client $client,
        Request $request,
        Computer $computer,
        AccountService $accountService
    ): Response {
        $account = new Account();
        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $account = $form->getData();
            $accountService->persistAccount($account, $employee, $computer);

            return $this->redirectToRoute(
                'pick_up_computer',
                array('id' => $employee->getId(),
                'clientId' => $client->getId())
            );
        }

        return $this->renderForm('pick-up/account/add/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/pick-up/{id}/{clientId}/{computerId}/account/edit/{accountId}", name="pick_up_account_edit")
     * @Entity("client", expr="repository.find(clientId)")
     * @Entity("computer", expr="repository.find(computerId)")
     * @Entity("account", expr="repository.find(accountId)")
     */
    public function editAccount(
        Employee $employee,
        Client $client,
        Computer $computer,
        Account $account,
        Request $request,
        AccountService $accountService
    ): Response {
        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accountService->updateAccount();

            return $this->redirectToRoute(
                'pick_up_computer',
                array('id' => $employee->getId(),
                'clientId' => $client->getId())
            );
        }

        return $this->renderForm('pick-up/account/edit/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/pick-up/{id}/{clientId}/{computerId}/account/remove/{accountId}", name="pick_up_account_remove")
     * @Entity("client", expr="repository.find(clientId)")
     * @Entity("computer", expr="repository.find(computerId)")
     * @Entity("account", expr="repository.find(accountId)")
     */
    public function removeAccount(
        Employee $employee,
        Client $client,
        Computer $computer,
        Account $account,
        Request $request,
        AccountService $accountService
    ): Response {
        $accountService->removeAccount($account);
        return $this->json(['code' => 200, 'message' => 'Compte supprimé'], 200);
    }
}
