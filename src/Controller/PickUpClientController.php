<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Employee;
use App\Form\ClientType;
use App\Repository\ClientRepository;
use App\Service\ClientService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;

class PickUpClientController extends AbstractController
{
    /**
     * @Route("/pick-up/{id}/client", name="pick_up_client")
     */
    public function index(Employee $employee, ClientRepository $clientRepository): Response
    {
        return $this->renderForm('pick-up/client/index.html.twig', [
            'clients' => $clientRepository->getLastFour(),
            'employee' => $employee,
        ]);
    }

    /**
     * @Route("/pick-up/{id}/client/add", name="pick_up_client_add")
     */
    public function addClient(Request $request, ClientService $clientService, Employee $employee): Response
    {
        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $clientService->persistClient($client);

            return $this->redirectToRoute('pick_up_client', array('id' => $employee->getId()));
        }

        return $this->renderForm('pick-up/client/add/index.html.twig', [
            'form' => $form,
            'employee' => $employee,
        ]);
    }

    /**
     * @Route("/pick-up/{id}/client/edit/{clientId}", name="pick_up_client_edit")
     * @Entity("client", expr="repository.find(clientId)")
     */
    public function editClient(
        Request $request,
        ClientService $clientService,
        Employee $employee,
        Client $client
    ): Response {
        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $clientService->updateClient();

            return $this->redirectToRoute('pick_up_client', array('id' => $employee->getId()));
        }

        return $this->renderForm('pick-up/client/add/index.html.twig', [
            'form' => $form,
            'employee' => $employee,
        ]);
    }

    /**
     * @Route("/pick-up/client/search", name="pick_up_client_search")
     */
    public function searchClient(
        Request $request,
        ClientRepository $clientRepository
    ): Response {
        $json = json_decode($request->getContent());
        $clients = $clientRepository->getByNameAndLastname($json->data);
        $data = [];
        foreach ($clients as $client) {
            array_push(
                $data,
                array(
                    'name' => $client->getLastname() . ' ' . $client->getFirstname(),
                    'address' => $client->getAddress() . ', ' . $client->getCity() . ' ' . $client->getPostcode(),
                    'mail' => $client->getMail(),
                    'phone' => $client->getPhone(),
                    'id' => $client->getId(),
                    'computers' => count($client->getComputers())
                )
            );
        }
        return $this->json(['code' => 200, 'data' => $data], 200);
    }
}
