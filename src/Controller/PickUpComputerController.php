<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Computer;
use App\Entity\Employee;
use App\Form\ComputerType;
use App\Repository\FixingRepository;
use App\Service\ComputerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\Request;

class PickUpComputerController extends AbstractController
{
    /**
     * @Route("/pick-up/{id}/{clientId}/computer", name="pick_up_computer")
     * @Entity("client", expr="repository.find(clientId)")
     */
    public function index(
        Employee $employee,
        Client $client,
        FixingRepository $fixingRepository
    ): Response {
        $computers = $client->getComputers();
        $fixings = [];
        foreach ($computers as $computer) {
            $tmp = $fixingRepository->getByComputer($computer->getId());
            if (isset($tmp[0])) {
                array_push($fixings, $tmp[0]);
            }
        }

        return $this->render('pick-up/computer/index.html.twig', [
            'client' => $client,
            'computers' => $computers,
            'employee' => $employee,
            'fixings' => $fixings
        ]);
    }

    /**
     * @Route("/pick-up/{id}/{clientId}/computer/add", name="pick_up_computer_add")
     * @Entity("client", expr="repository.find(clientId)")
     */
    public function addComputer(
        Employee $employee,
        Client $client,
        ComputerService $computerService,
        Request $request
    ): Response {
        $computer = new Computer();
        $form = $this->createForm(ComputerType::class, $computer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $computer = $form->getData();
            $computerService->persistComputer($computer, $client);

            return $this->redirectToRoute(
                'pick_up_computer',
                array('id' => $employee->getId(),
                'clientId' => $client->getId())
            );
        }

        return $this->renderForm('pick-up/computer/add/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/pick-up/{id}/{clientId}/computer/edit/{computerId}", name="pick_up_computer_edit")
     * @Entity("client", expr="repository.find(clientId)")
     * @Entity("computer", expr="repository.find(computerId)")
     */
    public function editComputer(
        Employee $employee,
        Client $client,
        ComputerService $computerService,
        Request $request,
        Computer $computer
    ): Response {
        $form = $this->createForm(ComputerType::class, $computer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $computerService->updateComputer();

            return $this->redirectToRoute(
                'pick_up_computer',
                array('id' => $employee->getId(),
                'clientId' => $client->getId())
            );
        }

        return $this->renderForm('pick-up/computer/add/index.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/pick-up/{id}/{clientId}/computer/remove/{computerId}", name="pick_up_computer_remove")
     * @Entity("client", expr="repository.find(clientId)")
     * @Entity("computer", expr="repository.find(computerId)")
     */
    public function removeComputer(
        Employee $employee,
        Client $client,
        ComputerService $computerService,
        Request $request,
        Computer $computer
    ): Response {
        $computerService->removeComputer($computer);
        return $this->json(['code' => 200, 'message' => 'computer deleted'], 200);
    }
}
