<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PickUpEmployeeController extends AbstractController
{
    /**
     * @Route("/pick-up/employee", name="pick_up_employee")
     */
    public function index(EmployeeRepository $employeeRepository): Response
    {
        return $this->render('pick-up/employee/index.html.twig', [
            'employees' => $employeeRepository->getEmployee(),
        ]);
    }

    /**
     * @Route("/pick-up/employee={id}", name="pick_up_choice")
     */
    public function pickupTypeChoice(Employee $employee): Response
    {
        return $this->render('pick-up/employee/choice.html.twig', [
            "employee" => $employee
        ]);
    }
}
