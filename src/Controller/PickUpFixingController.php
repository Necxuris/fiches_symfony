<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\Computer;
use App\Entity\Employee;
use App\Entity\Fixing;
use App\Entity\Service;
use App\Form\FixingType;
use App\Repository\ServiceRepository;
use App\Service\FixingService;
use App\Service\MercureService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\Request;

class PickUpFixingController extends AbstractController
{
    /**
     * @Route("/pick-up/{id}/{clientId}/{computerId}/fixing", name="pick_up_fixing")
     * @Entity("client", expr="repository.find(clientId)")
     * @Entity("computer", expr="repository.find(computerId)")
     */
    public function index(
        Employee $employee,
        Client $client,
        Request $request,
        Computer $computer,
        FixingService $fixingService,
        MercureService $mercureService
    ): Response {
        $fixing = new Fixing();
        $form = $this->createForm(FixingType::class, $fixing);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $fixing = $form->getData();
            $fixingService->persistfixing($fixing, $employee, $computer, $mercureService);

            return $this->redirectToRoute('pick_up_logout');
        }

        return $this->renderForm('pick-up/fixing/index.html.twig', [
            'form' => $form,
        ]);
    }
}
