<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Repository\FixingRepository;
use App\Repository\ServiceRepository;
use App\Service\FixingService;
use App\Service\RecordsFixingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PickUpWarantlyController extends AbstractController
{
    /**
     * @Route("/pick-up/{id}/warantly", name="pick_up_warantly")
     */
    public function index(
        Employee $employee,
        FixingRepository $fixingRepository,
        Request $request,
        FixingService $fixingService,
        ServiceRepository $serviceRepository,
        RecordsFixingService $recordsFixingService
    ): Response {
        $form = $request->request->all();

        if ($form != null) {
            if (!isset($form['fixing'])) {
                return $this->render('pick-up/warantly/index.html.twig', [
                    'controller_name' => 'FixFinishFixingController',
                    'employee' => $employee,
                    'fixings' => $fixingRepository->getByWarantly(true),
                ]);
            }
            $fixing = $fixingRepository->findOneBy(['id' => $form['fixing']]);
            $fixingService->pickUpWarantly($fixing, $employee, $form, $serviceRepository, $recordsFixingService);
        }
        return $this->render('pick-up/warantly/index.html.twig', [
            'controller_name' => 'FixFinishFixingController',
            'employee' => $employee,
            'fixings' => $fixingRepository->getByWarantly(true),
        ]);
    }
}
