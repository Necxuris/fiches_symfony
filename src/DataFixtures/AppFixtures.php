<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface as HasherUserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $hasher;

    public function __construct(HasherUserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $employee = new Employee();

        $employee->setEmail('employee@microfun.fr')
                 ->setRoles('ROLES_ADMIN')
                 ->setFirstname($faker->firstName())
                 ->setLastname($faker->lastName())
                 ->setPicture('https://picsum.photos/200/300')
                 ->setInitials('TE')
                 ->setCreationDate(new DateTime());

        $password = $this->hasher->hashPassword($employee, 'password');
        $employee->setPassword($password);

        $manager->persist($employee);

        $manager->flush();
    }
}
