<?php

namespace App\Entity;

use App\Repository\ActionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ActionRepository::class)
 */
class Action
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $procedureNumber;

    /**
     * @ORM\Column(type="smallint")
     */
    private $actionNumber;

    /**
     * @ORM\ManyToOne(targetEntity=Fixing::class, inversedBy="actions", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fixing;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $employee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $procedureGroupNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProcedureNumber(): ?int
    {
        return $this->procedureNumber;
    }

    public function setProcedureNumber(int $procedureNumber): self
    {
        $this->procedureNumber = $procedureNumber;

        return $this;
    }

    public function getActionNumber(): ?int
    {
        return $this->actionNumber;
    }

    public function setActionNumber(int $actionNumber): self
    {
        $this->actionNumber = $actionNumber;

        return $this;
    }

    public function getFixing(): ?Fixing
    {
        return $this->fixing;
    }

    public function setFixing(?Fixing $fixing): self
    {
        $this->fixing = $fixing;

        return $this;
    }

    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    public function setEmployee(?Employee $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getProcedureGroupNumber(): ?int
    {
        return $this->procedureGroupNumber;
    }

    public function setProcedureGroupNumber(?int $procedureGroupNumber): self
    {
        $this->procedureGroupNumber = $procedureGroupNumber;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
