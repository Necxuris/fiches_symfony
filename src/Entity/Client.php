<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 * @ORM\Table(name="client", indexes={
 *     @ORM\Index(name="idx_name", columns={"lastname", "firstname"}),
 * })
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $phone;

    /**
     * @ORM\Column(type="boolean")
     */
    private $newsletter;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allow_data_storage;

    /**
     * @ORM\Column(type="integer")
     */
    private $identification_code;

    /**
     * @ORM\Column(type="date")
     */
    private $creation_date;

    /**
     * @ORM\OneToMany(targetEntity=Computer::class, mappedBy="client", orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    private $computers;

    /**
     * @ORM\OneToMany(targetEntity=RecordsFixing::class, mappedBy="client")
     */
    private $recordsFixings;

    public function __construct()
    {
        $this->computers = new ArrayCollection();
        $this->recordsFixings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(bool $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getAllowDataStorage(): ?bool
    {
        return $this->allow_data_storage;
    }

    public function setAllowDataStorage(bool $allow_data_storage): self
    {
        $this->allow_data_storage = $allow_data_storage;

        return $this;
    }

    public function getIdentificationCode(): ?int
    {
        return $this->identification_code;
    }

    public function setIdentificationCode(int $identification_code): self
    {
        $this->identification_code = $identification_code;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creation_date;
    }

    public function setCreationDate(\DateTimeInterface $creation_date): self
    {
        $this->creation_date = $creation_date;

        return $this;
    }

    /**
     * @return Collection|Computer[]
     */
    public function getComputers(): Collection
    {
        return $this->computers;
    }

    public function addComputer(Computer $computer): self
    {
        if (!$this->computers->contains($computer)) {
            $this->computers[] = $computer;
            $computer->setClient($this);
        }

        return $this;
    }

    public function removeComputer(Computer $computer): self
    {
        if ($this->computers->removeElement($computer)) {
            // set the owning side to null (unless already changed)
            if ($computer->getClient() === $this) {
                $computer->setClient(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RecordsFixing>
     */
    public function getRecordsFixings(): Collection
    {
        return $this->recordsFixings;
    }

    public function addRecordsFixing(RecordsFixing $recordsFixing): self
    {
        if (!$this->recordsFixings->contains($recordsFixing)) {
            $this->recordsFixings[] = $recordsFixing;
            $recordsFixing->setClient($this);
        }

        return $this;
    }

    public function removeRecordsFixing(RecordsFixing $recordsFixing): self
    {
        if ($this->recordsFixings->removeElement($recordsFixing)) {
            // set the owning side to null (unless already changed)
            if ($recordsFixing->getClient() === $this) {
                $recordsFixing->setClient(null);
            }
        }

        return $this;
    }
}
