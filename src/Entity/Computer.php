<?php

namespace App\Entity;

use App\Repository\ComputerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComputerRepository::class)
 */
class Computer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brandPicture;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="computers", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @ORM\OneToMany(targetEntity=Account::class, mappedBy="computer", orphanRemoval=true)
     */
    private $accounts;

    /**
     * @ORM\OneToMany(targetEntity=RecordsFixing::class, mappedBy="computer")
     */
    private $recordsFixings;

    public function __construct()
    {
        $this->accounts = new ArrayCollection();
        $this->recordsFixings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getBrandPicture(): ?string
    {
        return $this->brandPicture;
    }

    public function setBrandPicture(?string $brandPicture): self
    {
        $this->brandPicture = $brandPicture;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Collection|Account[]
     */
    public function getAccounts(): Collection
    {
        return $this->accounts;
    }

    public function addAccount(Account $account): self
    {
        if (!$this->accounts->contains($account)) {
            $this->accounts[] = $account;
            $account->setComputer($this);
        }

        return $this;
    }

    public function removeAccount(Account $account): self
    {
        if ($this->accounts->removeElement($account)) {
            // set the owning side to null (unless already changed)
            if ($account->getComputer() === $this) {
                $account->setComputer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, RecordsFixing>
     */
    public function getRecordsFixings(): Collection
    {
        return $this->recordsFixings;
    }

    public function addRecordsFixing(RecordsFixing $recordsFixing): self
    {
        if (!$this->recordsFixings->contains($recordsFixing)) {
            $this->recordsFixings[] = $recordsFixing;
            $recordsFixing->setComputer($this);
        }

        return $this;
    }

    public function removeRecordsFixing(RecordsFixing $recordsFixing): self
    {
        if ($this->recordsFixings->removeElement($recordsFixing)) {
            // set the owning side to null (unless already changed)
            if ($recordsFixing->getComputer() === $this) {
                $recordsFixing->setComputer(null);
            }
        }

        return $this;
    }
}
