<?php

namespace App\Entity;

use App\Repository\FixingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FixingRepository::class)
 */
class Fixing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $creationDateTime;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $status;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDateTime;

    /**
     * @ORM\OneToOne(targetEntity=Computer::class, cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $computer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderNumber;

    /**
     * @ORM\OneToMany(targetEntity=Action::class, mappedBy="fixing", orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE") 
     */
    private $actions;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="fixing", orphanRemoval=true)
     * @ORM\JoinColumn(onDelete="CASCADE") 
     */
    private $orders;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class, inversedBy="fixingsTaken")
     * @ORM\JoinColumn(nullable=false)
     */
    private $creationEmployee;

    /**
     * @ORM\ManyToMany(targetEntity=Service::class, inversedBy="fixings")
     */
    private $Services;

    /**
     * @ORM\ManyToMany(targetEntity=Material::class)
     */
    private $Materials;

    /**
     * @ORM\ManyToMany(targetEntity=Procedure::class)
     */
    private $procedures;

    /**
     * @ORM\Column(type="boolean")
     */
    private $start;

    /**
     * @ORM\Column(type="boolean")
     */
    private $os;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $damage;

    /**
     * @ORM\ManyToMany(targetEntity=ProcedureGroup::class)
     */
    private $procedureGroups;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class)
     */
    private $endEmployee;

    /**
     * @ORM\Column(type="boolean")
     */
    private $warantly;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paid;

    /**
     * @ORM\Column(type="boolean")
     */
    private $computerInShop;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $problem;

    /**
     * @ORM\OneToOne(targetEntity=FixingTool::class, cascade={"persist", "remove"})
     */
    private $fixingTool;

    public function __construct()
    {
        $this->actions = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->Services = new ArrayCollection();
        $this->Materials = new ArrayCollection();
        $this->procedures = new ArrayCollection();
        $this->procedureGroups = new ArrayCollection();
    }

    public function __toString()
    {
        return ('fixing');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreationDateTime(): ?\DateTimeInterface
    {
        return $this->creationDateTime;
    }

    public function setCreationDateTime(\DateTimeInterface $creationDateTime): self
    {
        $this->creationDateTime = $creationDateTime;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEndDateTime(): ?\DateTimeInterface
    {
        return $this->endDateTime;
    }

    public function setEndDateTime(?\DateTimeInterface $endDateTime): self
    {
        $this->endDateTime = $endDateTime;

        return $this;
    }

    public function getComputer(): ?Computer
    {
        return $this->computer;
    }

    public function setComputer(Computer $computer): self
    {
        $this->computer = $computer;

        return $this;
    }

    public function getOrderNumber(): ?int
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(?int $orderNumber): self
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * @return Collection|Action[]
     */
    public function getActions(): Collection
    {
        return $this->actions;
    }

    public function addAction(Action $action): self
    {
        if (!$this->actions->contains($action)) {
            $this->actions[] = $action;
            $action->setFixing($this);
        }

        return $this;
    }

    public function removeAction(Action $action): self
    {
        if ($this->actions->removeElement($action)) {
            // set the owning side to null (unless already changed)
            if ($action->getFixing() === $this) {
                $action->setFixing(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setFixing($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getFixing() === $this) {
                $order->setFixing(null);
            }
        }

        return $this;
    }

    public function getCreationEmployee(): ?Employee
    {
        return $this->creationEmployee;
    }

    public function setCreationEmployee(?Employee $creationEmployee): self
    {
        $this->creationEmployee = $creationEmployee;

        return $this;
    }

    /**
     * @return Collection|Service[]
     */
    public function getServices(): Collection
    {
        return $this->Services;
    }

    public function addService(Service $service): self
    {
        if (!$this->Services->contains($service)) {
            $this->Services[] = $service;
        }

        return $this;
    }

    public function removeService(Service $service): self
    {
        $this->Services->removeElement($service);

        return $this;
    }

    /**
     * @return Collection|Material[]
     */
    public function getMaterials(): Collection
    {
        return $this->Materials;
    }

    public function addMaterial(Material $material): self
    {
        if (!$this->Materials->contains($material)) {
            $this->Materials[] = $material;
        }

        return $this;
    }

    public function removeMaterial(Material $material): self
    {
        $this->Materials->removeElement($material);

        return $this;
    }

    /**
     * @return Collection|Procedure[]
     */
    public function getProcedures(): Collection
    {
        return $this->procedures;
    }

    public function addProcedure(Procedure $procedure): self
    {
        if (!$this->procedures->contains($procedure)) {
            $this->procedures[] = $procedure;
        }

        return $this;
    }

    public function removeProcedure(Procedure $procedure): self
    {
        $this->procedures->removeElement($procedure);

        return $this;
    }

    public function getStart(): ?bool
    {
        return $this->start;
    }

    public function setStart(bool $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getOs(): ?bool
    {
        return $this->os;
    }

    public function setOs(bool $os): self
    {
        $this->os = $os;

        return $this;
    }

    public function getDamage(): ?string
    {
        return $this->damage;
    }

    public function setDamage(?string $damage): self
    {
        $this->damage = $damage;

        return $this;
    }

    /**
     * @return Collection<int, ProcedureGroup>
     */
    public function getProcedureGroups(): Collection
    {
        return $this->procedureGroups;
    }

    public function addProcedureGroup(ProcedureGroup $procedureGroup): self
    {
        if (!$this->procedureGroups->contains($procedureGroup)) {
            $this->procedureGroups[] = $procedureGroup;
        }

        return $this;
    }

    public function removeProcedureGroup(ProcedureGroup $procedureGroup): self
    {
        $this->procedureGroups->removeElement($procedureGroup);

        return $this;
    }

    public function getEndEmployee(): ?Employee
    {
        return $this->endEmployee;
    }

    public function setEndEmployee(?Employee $endEmployee): self
    {
        $this->endEmployee = $endEmployee;

        return $this;
    }

    public function getWarantly(): ?bool
    {
        return $this->warantly;
    }

    public function setWarantly(bool $warantly): self
    {
        $this->warantly = $warantly;

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getComputerInShop(): ?bool
    {
        return $this->computerInShop;
    }

    public function setComputerInShop(bool $computerInShop): self
    {
        $this->computerInShop = $computerInShop;

        return $this;
    }

    public function getProblem(): ?string
    {
        return $this->problem;
    }

    public function setProblem(string $problem): self
    {
        $this->problem = $problem;

        return $this;
    }

    public function getFixingTool(): ?FixingTool
    {
        return $this->fixingTool;
    }

    public function setFixingTool(?FixingTool $fixingTool): self
    {
        $this->fixingTool = $fixingTool;

        return $this;
    }
}
