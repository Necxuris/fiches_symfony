<?php

namespace App\Entity;

use App\Repository\FixingToolRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FixingToolRepository::class)
 */
class FixingTool
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $data = [];

    public function __toString()
    {
        return ('fixingTool');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData(): array
    {
        $data = $this->data;
        return array_unique($data);
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }
}
