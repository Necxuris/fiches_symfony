<?php

namespace App\Entity;

use App\Repository\ProcedureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProcedureRepository::class)
 * @ORM\Table(name="`procedure`")
 */
class Procedure
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     */
    private $elements = [];

    /**
     * @ORM\ManyToOne(targetEntity=ProcedureGroup::class, inversedBy="procedures")
     */
    private $procedureGroup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $procedureGroupOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getElements(): ?array
    {
        return $this->elements;
    }

    public function setElements(array $elements): self
    {
        $this->elements = $elements;

        return $this;
    }

    public function getProcedureGroup(): ?ProcedureGroup
    {
        return $this->procedureGroup;
    }

    public function setProcedureGroup(?ProcedureGroup $procedureGroup): self
    {
        $this->procedureGroup = $procedureGroup;

        return $this;
    }

    public function getProcedureGroupOrder(): ?int
    {
        return $this->procedureGroupOrder;
    }

    public function setProcedureGroupOrder(?int $procedureGroupOrder): self
    {
        $this->procedureGroupOrder = $procedureGroupOrder;

        return $this;
    }
}
