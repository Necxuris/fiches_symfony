<?php

namespace App\Entity;

use App\Repository\ProcedureGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProcedureGroupRepository::class)
 */
class ProcedureGroup
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Procedure::class, mappedBy="procedureGroup", fetch="EAGER")
     */
    private $procedures;

    public function __construct()
    {
        $this->procedures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Procedure[]
     */
    public function getProcedures(): Collection
    {
        return $this->procedures;
    }

    public function addProcedure(Procedure $procedure): self
    {
        if (!$this->procedures->contains($procedure)) {
            $this->procedures[] = $procedure;
            $procedure->setProcedureGroup($this);
        }

        return $this;
    }

    public function removeProcedure(Procedure $procedure): self
    {
        if ($this->procedures->removeElement($procedure)) {
            // set the owning side to null (unless already changed)
            if ($procedure->getProcedureGroup() === $this) {
                $procedure->setProcedureGroup(null);
            }
        }

        return $this;
    }
}
