<?php

namespace App\Entity;

use App\Repository\RecordsFixingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RecordsFixingRepository::class)
 */
class RecordsFixing
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="text")
     */
    private $comment;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $receiptNumber;

    /**
     * @ORM\OneToMany(targetEntity=RecordsService::class,
     *  mappedBy="recordsFixing",
     *  orphanRemoval=true,
     *  cascade={"persist"}
     * )
     */
    private $recordsServices;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $endEmployee;

    /**
     * @ORM\ManyToOne(targetEntity=Client::class, inversedBy="recordsFixings")
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity=Computer::class, inversedBy="recordsFixings")
     */
    private $computer;

    public function __construct()
    {
        $this->recordsServices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getReceiptNumber(): ?int
    {
        return $this->receiptNumber;
    }

    public function setReceiptNumber(?int $receiptNumber): self
    {
        $this->receiptNumber = $receiptNumber;

        return $this;
    }

    /**
     * @return Collection|RecordsService[]
     */
    public function getRecordsServices(): Collection
    {
        return $this->recordsServices;
    }

    public function addRecordsService(RecordsService $recordsService): self
    {
        if (!$this->recordsServices->contains($recordsService)) {
            $this->recordsServices[] = $recordsService;
            $recordsService->setRecordsFixing($this);
        }

        return $this;
    }

    public function removeRecordsService(RecordsService $recordsService): self
    {
        if ($this->recordsServices->removeElement($recordsService)) {
            // set the owning side to null (unless already changed)
            if ($recordsService->getRecordsFixing() === $this) {
                $recordsService->setRecordsFixing(null);
            }
        }

        return $this;
    }

    public function getEndEmployee(): ?Employee
    {
        return $this->endEmployee;
    }

    public function setEndEmployee(?Employee $endEmployee): self
    {
        $this->endEmployee = $endEmployee;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getComputer(): ?Computer
    {
        return $this->computer;
    }

    public function setComputer(?Computer $computer): self
    {
        $this->computer = $computer;

        return $this;
    }
}
