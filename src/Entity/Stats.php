<?php

namespace App\Entity;

use App\Repository\StatsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StatsRepository::class)
 */
class Stats
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $clients;

    /**
     * @ORM\Column(type="integer")
     */
    private $fixings;

    /**
     * @ORM\Column(type="integer")
     */
    private $total;

    /**
     * @ORM\Column(type="integer")
     */
    private $labor;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClients(): ?int
    {
        return $this->clients;
    }

    public function setClients(int $clients): self
    {
        $this->clients = $clients;

        return $this;
    }

    public function getFixings(): ?int
    {
        return $this->fixings;
    }

    public function setFixings(int $fixings): self
    {
        $this->fixings = $fixings;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getLabor(): ?int
    {
        return $this->labor;
    }

    public function setLabor(int $labor): self
    {
        $this->labor = $labor;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }
}
