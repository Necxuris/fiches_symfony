<?php

namespace App\Form;

use App\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', TextType::class, [
                'attr' => array(
                    'autofocus' => true
                ),
                'label' => 'Nom',
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom',
            ])
            ->add('postcode', TextType::class, [
                'label' => 'Code postal',
                'attr' => array(
                    'autocomplete' => 'off'
                ),
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville',
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse',
                'attr' => array(
                    'autocomplete' => 'off'
                ),
            ])
            ->add('mail', EmailType::class, [
                'required' => false,
                'label' => 'Adresse mail',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Téléphone',
            ])
            ->add('newsletter', ChoiceType::class, [
                'choices' => array(
                    'Oui' => true,
                    'Non' => false
                ),
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('allow_data_storage', ChoiceType::class, [
                'choices' => array(
                    'Oui' => true,
                    'Non' => false
                ),
                'label' => 'Autorisation stockage des données dans la base de données',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
