<?php

namespace App\Form;

use App\Entity\Computer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ComputerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('brand', TextType::class, [
                'label' => 'Marque',
                'attr' => [
                    'placeholder' => '...',
                ],
            ])
            ->add('type', ChoiceType::class, [
                'choices' => array(
                    'PC portable' => 'PC portable',
                    'PC fixe' => 'PC fixe',
                    'Tablette' => 'Tablette',
                    'Imprimante' => 'Imprimante',
                    'Stockage externe (HDD externe, clé USB, carte SD, NAS, ...)' => 'Stockage externe',
                    'Autre matériel' => 'Autre matériel'
                ),
                'label' => 'Type de matériel',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('model', TextType::class, [
                'label' => 'Modèle',
                'attr' => [
                    'placeholder' => '...',
                ],
            ])
            ->add('password', TextType::class, [
                'required' => false,
                'label' => 'Mot de passe',
            ])
            ->add('envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Computer::class,
        ]);
    }
}
