<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class)
            ->add('password', PasswordType::class)
            ->add('lastname', TextType::class)
            ->add('firstname', TextType::class)
            ->add('roles', ChoiceType::class, [
                'choices' => array(
                    'User ( /!\ à n\'appliquer que sur l\'utilisateur de prise en charge)' => 'ROLE_USER',
                    'Commercial (prise en charge + backoffice tarif et stats)' => 'ROLE_COMMERCIAL',
                    'Technicien (prise en charge + dépannage)' => 'ROLE_TECH',
                    'Admin' => 'ROLE_ADMIN'
                ),
                'label' => 'Quel role ?',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('envoyer', SubmitType::class)
        ;

        $builder->get('roles')
        ->addModelTransformer(new CallbackTransformer(
            function ($rolesArray) {
                 // transform the array to a string
                 return count($rolesArray) ? $rolesArray[0] : null;
            },
            function ($rolesString) {
                 // transform the string back to an array
                 return [$rolesString];
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
        ]);
    }
}
