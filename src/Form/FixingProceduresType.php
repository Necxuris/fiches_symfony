<?php

namespace App\Form;

use App\Entity\Fixing;
use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use App\Repository\ProcedureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FixingProceduresType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('procedureGroups', EntityType::class, [
                'class' => ProcedureGroup::class,
                'label' => 'Quels groupes souhaitez vous ajouter ?',
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('procedures', EntityType::class, [
                'class' => Procedure::class,
                'label' => 'Quelles procédures souhaitez vous ajouter ?',
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
                'by_reference' => false,
                'query_builder' => function (ProcedureRepository $pr) {
                    return $pr->createQueryBuilder('p')
                        ->where('p.procedureGroup is NULL')
                    ;
                },
            ])
            ->add('envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Fixing::class,
        ]);
    }
}
