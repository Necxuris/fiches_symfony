<?php

namespace App\Form;

use App\Entity\Fixing;
use App\Entity\Material;
use App\Entity\Service;
use App\Repository\ServiceRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class FixingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', ChoiceType::class, [
                'choices' => array(
                    'Oui' => true,
                    'Non' => false
                ),
                'label' => 'Le PC démarre t-il ?',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('os', ChoiceType::class, [
                'choices' => array(
                    'Oui' => true,
                    'Non' => false
                ),
                'label' => 'L\'OS démarre t-il ?',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('damage', TextType::class, [
                'required' => false,
                'label' => 'Dommages matériels',
            ])
            ->add('problem', TextType::class, [
                'label' => 'Pannes',
            ])
            ->add('services', EntityType::class, [
                'class' => Service::class,
                'choice_label' => function (Service $service) {
                    return $service->getType() . $service->getNumber()
                        . ' - ' . $service->getName()
                        . ' - ' . $service->getPrice() . '€';
                },
                'placeholder' => 'Tarifs et prestations',
                'autocomplete' => true,
                'multiple' => true,
                'by_reference' => false,
            ])
            ->add('materials', EntityType::class, [
                'class' => Material::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
                'label' => 'Matériel déposé',
            ])
            ->add('paid', ChoiceType::class, [
                'choices' => array(
                    'Oui' => true,
                    'Non' => false
                ),
                'label' => 'Le dépannage est-il déjà payé ?',
                'required' => true,
                'multiple' => false,
                'expanded' => true,
            ])
            ->add('envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fixing::class,
        ]);
    }
}
