<?php

namespace App\Form;

use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use App\Repository\ProcedureRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProcedureGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $id = $options['data']->getId();
        if ($id == null) {
            $id = 0;
        }
        $builder
            ->add('name', TextType::class)
            ->add('procedures', EntityType::class, [
                'class' => Procedure::class,
                'choice_label' => 'name',
                'multiple' => true,
                'expanded' => true,
                'by_reference' => false,
                'query_builder' => function (ProcedureRepository $pr) use ($id) {
                    return $pr->createQueryBuilder('p')
                       ->setParameter('id', $id)
                        ->where('p.procedureGroup is NULL OR p.procedureGroup = :id')
                    ;
                },
            ])
            ->add('envoyer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProcedureGroup::class,
        ]);
    }
}
