<?php

namespace App\Repository;

use App\Entity\Action;
use App\Entity\Fixing;
use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Action|null find($id, $lockMode = null, $lockVersion = null)
 * @method Action|null findOneBy(array $criteria, array $orderBy = null)
 * @method Action[]    findAll()
 * @method Action[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Action::class);
    }

    public function getActionsByProcedureAndFixing(Procedure $procedure, Fixing $fixing)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('fixingId', $fixing->getId())
            ->setParameter('procedureId', $procedure->getId())
            ->where('a.fixing = :fixingId AND a.procedureNumber = :procedureId')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getActionsByProcedureGroupAndFixing(ProcedureGroup $procedureGroup, Fixing $fixing)
    {
        return $this->createQueryBuilder('a')
            ->setParameter('fixingId', $fixing->getId())
            ->setParameter('procedureGroupId', $procedureGroup->getId())
            ->where('a.fixing = :fixingId AND a.procedureGroupNumber = :procedureGroupId')
            ->getQuery()
            ->getResult()
        ;
    }
    // /**
    //  * @return Action[] Returns an array of Action objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Action
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
