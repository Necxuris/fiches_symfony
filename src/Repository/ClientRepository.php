<?php

namespace App\Repository;

use App\Entity\Client;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function getLastFour()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.id', 'DESC')
            ->setMaxResults(4)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByNameAndLastname(array $data)
    {

        $qb = $this->createQueryBuilder('c');

        $orExpr = $qb->expr()->orX();

        for ($i = 0; $i < count($data); $i++) {
            $orExpr->add($qb->expr()->like('c.lastname', "?$i"));
            $orExpr->add($qb->expr()->like('c.firstname', "?$i"));

            $qb->setParameter($i, '%' . $data[$i] . '%');
        }

        $qb->where($orExpr);
        $qb->setMaxResults(25);
        return $qb->getQuery()->getResult();
    }

    public function getNumberClientFromMonth(DateTime $date)
    {
        $string = '%' . $date->format('Y-m') . '%';
        $result = $this->createQueryBuilder('c')
            ->select('count(c.id)')
            ->where('c.creation_date LIKE :date')
            ->setParameter('date', $string)
            ->getQuery()
            ->getResult()
        ;
        return $result;
    }


    // /**
    //  * @return Client[] Returns an array of Client objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Client
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
