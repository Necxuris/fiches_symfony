<?php

namespace App\Repository;

use App\Entity\Crowd;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Crowd>
 *
 * @method Crowd|null find($id, $lockMode = null, $lockVersion = null)
 * @method Crowd|null findOneBy(array $criteria, array $orderBy = null)
 * @method Crowd[]    findAll()
 * @method Crowd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CrowdRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Crowd::class);
    }

    public function add(Crowd $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Crowd $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getCrowdFromMonth(DateTime $date)
    {
        $string = '%' . $date->format('Y-m') . '%';
        $result = $this->createQueryBuilder('c')
            ->where('c.date LIKE :date')
            ->setParameter('date', $string)
            ->getQuery()
            ->getResult()
        ;
        return $result;
    }
//    /**
//     * @return Crowd[] Returns an array of Crowd objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Crowd
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
