<?php

namespace App\Repository;

use App\Entity\Fixing;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Fixing|null find($id, $lockMode = null, $lockVersion = null)
 * @method Fixing|null findOneBy(array $criteria, array $orderBy = null)
 * @method Fixing[]    findAll()
 * @method Fixing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FixingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fixing::class);
    }

    public function getByComputer(int $id)
    {
        return $this->createQueryBuilder('c')
            ->setParameter('id', $id)
            ->where('c.computer = :id')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByWarantly(bool $warantly)
    {
        return $this->createQueryBuilder('w')
            ->setParameter('warantly', $warantly)
            ->where('w.warantly = :warantly')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getByStatus(string $status)
    {
        return $this->createQueryBuilder('s')
            ->setParameter('status', $status)
            ->setParameter('warantly', false)
            ->where('s.status = :status AND s.warantly = :warantly')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getFixingFromMonth(DateTime $date)
    {
        $string = '%' . $date->format('Y-m') . '%';
        $result = $this->createQueryBuilder('f')
            ->where('f.endDateTime LIKE :date')
            ->setParameter('date', $string)
            ->getQuery()
            ->getResult()
        ;
        return $result;
    }

    public function countFixingByDay(DateTime $date)
    {
        $result = $this->createQueryBuilder('f')
            ->select('count(f.id)')
            ->where('f.creationDateTime = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->getQuery()
            ->getResult()
        ;
        return $result;
    }


    // public function getServicesFromArray(Array $fixings)
    // {
    //     $result = $this->createQueryBuilder('f')
    //         ->select('f.Services') 
    //         ->leftJoin('f.Services', 'service')
    //         ->where('f IN (:arrayOfFixing)')
    //         ->setParameter('arrayOfFixing', $fixings)
    //         ->getQuery()
    //         ->getResult();

    //     return ($result);
    // }

    // public function getFixings()
    // {
    //     return $this->createQueryBuilder('f')
    //         ->innerJoin('f.services','s')
    //         ->where('f.id = :)
    //         ->getQuery()
    //         ->getResult()
    //     ;
    // }

    // /**
    //  * @return Fixing[] Returns an array of Fixing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Fixing
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
