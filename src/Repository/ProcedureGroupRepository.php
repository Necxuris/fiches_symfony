<?php

namespace App\Repository;

use App\Entity\ProcedureGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProcedureGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcedureGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcedureGroup[]    findAll()
 * @method ProcedureGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcedureGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcedureGroup::class);
    }

    // /**
    //  * @return ProcedureGroup[] Returns an array of ProcedureGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcedureGroup
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
