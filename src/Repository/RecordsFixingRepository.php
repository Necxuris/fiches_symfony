<?php

namespace App\Repository;

use App\Entity\RecordsFixing;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RecordsFixing|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecordsFixing|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecordsFixing[]    findAll()
 * @method RecordsFixing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecordsFixingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecordsFixing::class);
    }

    public function getRecordsFixingsFromMonth(DateTime $date)
    {
        $string = '%' . $date->format('Y-m') . '%';
        $result = $this->createQueryBuilder('f')
            ->where('f.endDate LIKE :date')
            ->setParameter('date', $string)
            ->getQuery()
            ->getResult()
        ;
        return $result;
    }

    // /**
    //  * @return RecordsFixing[] Returns an array of RecordsFixing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecordsFixing
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
