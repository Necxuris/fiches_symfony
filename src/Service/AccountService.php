<?php

namespace App\Service;

use App\Entity\Account;
use App\Entity\Computer;
use App\Entity\Employee;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class AccountService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistAccount(Account $account, Employee $employee, Computer $computer): void
    {
        $account->setEmployee($employee);
        $account->setComputer($computer);
        $this->manager->persist($account);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateAccount(): void
    {
        $this->manager->flush();
    }

    public function removeAccount(Account $account): void
    {
        $this->manager->remove($account);
        $this->manager->flush();
    }
}
