<?php

namespace App\Service;

use App\Entity\Action;
use App\Entity\Employee;
use App\Entity\Fixing;
use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use App\Repository\ActionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ActionService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistAction(Action $action, Fixing $fixing, Employee $employee, object $json): void
    {
        $action->setFixing($fixing);
        $action->setEmployee($employee);
        $action->setProcedureNumber($json->procedureNumber);
        $action->setActionNumber($json->actionNumber);
        $action->setValue($json->value);
        $action->setProcedureGroupNumber($json->procedureGroupNumber);
        $action->setName($json->name);
        $this->manager->persist($action);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function getUniqueActions(ActionRepository $actionRepository, Fixing $fixing, Procedure $procedure): array
    {
        $actions = [];
        $tmp = $actionRepository->getActionsByProcedureAndFixing($procedure, $fixing);
        for ($i = 0; $i < count($tmp); $i++) {
            if (!in_array($tmp[$i]->getActionNumber(), $actions)) {
                array_push($actions, $tmp[$i]->getActionNumber());
            }
        }
        return $actions;
    }

    public function getUniqueActionsGroup(
        ActionRepository $actionRepository,
        Fixing $fixing,
        ProcedureGroup $procedureGroup
    ): array {
        $actions = [];
        $tmp = $actionRepository->getActionsByProcedureGroupAndFixing($procedureGroup, $fixing);
        for ($i = 0; $i < count($tmp); $i++) {
            if (!in_array(array($tmp[$i]->getProcedureNumber(), $tmp[$i]->getActionNumber()), $actions)) {
                array_push($actions, array($tmp[$i]->getProcedureNumber(), $tmp[$i]->getActionNumber()));
            }
        }
        return $actions;
    }
}
