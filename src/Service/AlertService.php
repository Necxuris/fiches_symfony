<?php

namespace App\Service;

use Bluerhinos\phpMQTT;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class AlertService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function alertMQTT(string $phone, string $route): void
    {
        $mqtt = new phpMQTT('192.168.0.243', 1883, 'phpMQTT-publisher');
        $firstNumber = substr($phone, 0, 2);
        if ($firstNumber != '06' && $firstNumber != '07') {
            $this->flash->add(
                $route . '_warning',
                'Client à appeler : ' . $phone
            );
            return;
        }
        if ($mqtt->connect()) {
            $mqtt->publish('topic/SMS', $phone, 0, false);
            $mqtt->close();
            $this->flash->add($route . '_success', 'Client prévenue avec succès');
            return;
        }
        $this->flash->add($route . '_error', 'Erreur de connection au serveur MQTT, SMS non envoyé');
        return;
    }

    public function alertMail(string $mail, string $route): void
    {
        /*
        if ($fixing->getComputer()->getClient()->getMail() != null) {
            $email = (new Email())
                ->to($fixing->getComputer()->getClient()->getMail())
                ->subject('test mail')
                ->text('viens chercher ton PC');
            $mailer->send($email);
        }*/
        return;
    }
}
