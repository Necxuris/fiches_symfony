<?php

namespace App\Service;

use App\Repository\ClientRepository;
use App\Repository\CrowdRepository;
use App\Repository\FixingRepository;
use App\Repository\RecordsFixingRepository;
use App\Repository\StatsRepository;
use DateTime;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

class ChartService
{
    private $statsService;
    private $statsRepository;
    private $clientRepository;
    private $fixingRepository;
    private $crowdRepository;
    private $recordsFixingRepository;

    public function __construct(
        StatsService $statsService,
        StatsRepository $statsRepository,
        ClientRepository $clientRepository,
        FixingRepository $fixingRepository,
        CrowdRepository $crowdRepository,
        RecordsFixingRepository $recordsFixingRepository
    ) {
        $this->statsService = $statsService;
        $this->statsRepository = $statsRepository;
        $this->clientRepository = $clientRepository;
        $this->fixingRepository = $fixingRepository;
        $this->crowdRepository = $crowdRepository;
        $this->recordsFixingRepository = $recordsFixingRepository;
    }

    public function createChartCart(DateTime $date): array
    {
        //récupération des stats de ce mois

        $actualDate = new DateTime(('now'));
        $statsActualMonth = $this->statsService->getStatsFromMonth(
            $actualDate,
            $this->clientRepository,
            $this->fixingRepository,
            $this->recordsFixingRepository
        );
        $statsMonth = $this->statsService->getStatsFromMonth(
            $date,
            $this->clientRepository,
            $this->fixingRepository,
            $this->recordsFixingRepository
        );

        //récupération des stats de cette année

        $stats = $this->statsRepository->getStatsFromYear($date);
        if ($stats == null) {
            $offset = 12;
        } else {
            $offset = intval($stats[0]->getDate()->format('m'));
        }
        $dataMoMoy = [];
        $dataMoy = [];
        $dataMo = [];
        $dataMat = [];
        $labels = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin',
        'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
        $labelsCrowd = [];
        $dataCrowd = [];

        //Déplacement des données pour les aligner lors de la première année

        for ($i = 1; $i < $offset; $i++) {
            array_push($dataMoy, 0);
            array_push($dataMo, 0);
            array_push($dataMat, 0);
            array_push($dataMoMoy, 0);
        }

        //extraction et traitement des stats récupérées

        foreach ($this->statsRepository->getStatsFromYear($date) as $stat) {
            array_push($dataMo, intval($stat->getLabor()));
            array_push($dataMat, intval($stat->getTotal() - $stat->getLabor()));
            if ($stat->getFixings() == 0) {
                array_push($dataMoy, intval($stat->getTotal()));
                array_push($dataMoMoy, intval($stat->getLabor()));
            } else {
                array_push($dataMoy, intval($stat->getTotal() / $stat->getFixings()));
                array_push($dataMoMoy, intval($stat->getLabor() / $stat->getFixings()));
            }
        }
        if (intval($date->format('Y')) == intval($actualDate->format('Y'))) {
            array_push($dataMo, intval($statsActualMonth->getLabor()));
            array_push($dataMat, intval($statsActualMonth->getTotal() - $statsActualMonth->getLabor()));
            if ($statsActualMonth->getFixings() == 0) {
                array_push($dataMoy, intval($statsActualMonth->getTotal()));
                array_push($dataMoMoy, intval($statsActualMonth->getLabor()));
            } else {
                array_push($dataMoy, intval($statsActualMonth->getTotal() / $statsActualMonth->getFixings()));
                array_push($dataMoMoy, intval($statsActualMonth->getLabor() / $statsActualMonth->getFixings()));
            }
        }

        foreach ($this->crowdRepository->getCrowdFromMonth($date) as $crowd) {
            array_push($labelsCrowd, $crowd->getDate()->format('d-m'));
            array_push($dataCrowd, $crowd->getNumber());
        }

        //création des json correspondant

        $jsonMoMoy = json_encode(array('labels' => $labels, 'data' => $dataMoMoy));
        $jsonMoy = json_encode(array('labels' => $labels, 'data' => $dataMoy));
        $jsonMo = json_encode(array('labels' => $labels, 'data' => $dataMo));
        $jsonMat = json_encode(array('labels' => $labels, 'data' => $dataMat));
        $jsonCrowd = json_encode(array('labels' => $labelsCrowd, 'data' => $dataCrowd));

        return array(
            'jsonMoMoy' => $jsonMoMoy,
            'jsonMoy' => $jsonMoy,
            'jsonMo' => $jsonMo,
            'jsonMat' => $jsonMat,
            'jsonCrowd' => $jsonCrowd
        );
    }


    public function getTopServices(array $fixings)
    {
        $unSortData = [];
        $labels = [];
        $data = [];
        foreach ($fixings as $fixing) {
            foreach ($fixing->getServices() as $service) {
                if ($service->getType() == 'P' || $service->getType() == 'F') {
                    $name = $service->getType() . $service->getNumber();
                    if (isset($unSortData[$name])) {
                        $unSortData[$name]++;
                    } else {
                        $unSortData[$name] = 1;
                    }
                }
            }
        }
        $maxData = (count($unSortData) > 25) ? 25 : count($unSortData);
        $max = 0;
        $label = '';
        for ($i = 0; $i < $maxData; $i++) {
            foreach ($unSortData as $key => $value) {
                if ($value > $max) {
                    $label = $key;
                    $max = $value;
                }
            }
            array_push($data, $max);
            array_push($labels, $label);
            unset($unSortData[$label]);
            $max = 0;
            $label = '';
        }
        return(json_encode(array('labels' => $labels, 'data' => $data)));
    }

    public function getTopRecordsServices(array $recordsFixings)
    {
        $unSortData = [];
        $labels = [];
        $data = [];
        foreach ($recordsFixings as $recordsFixing) {
            foreach ($recordsFixing->getRecordsServices() as $recordsService) {
                if ($recordsService->getType() == 'P' || $recordsService->getType() == 'F') {
                    $name = $recordsService->getType() . $recordsService->getNumber();
                    if (isset($unSortData[$name])) {
                        $unSortData[$name]++;
                    } else {
                        $unSortData[$name] = 1;
                    }
                }
            }
        }
        $maxData = (count($unSortData) > 25) ? 25 : count($unSortData);
        $max = 0;
        $label = '';
        for ($i = 0; $i < $maxData; $i++) {
            foreach ($unSortData as $key => $value) {
                if ($value > $max) {
                    $label = $key;
                    $max = $value;
                }
            }
            array_push($data, $max);
            array_push($labels, $label);
            unset($unSortData[$label]);
            $max = 0;
            $label = '';
        }
        return(json_encode(array('labels' => $labels, 'data' => $data)));
    }
}
