<?php

namespace App\Service;

use App\Entity\Client;
use App\Repository\ClientRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ClientService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistClient(Client $client): void
    {
        $client->setCreationDate(new DateTime('now'))
               ->setIdentificationCode(0)
               ->setLastname(strtolower($client->getLastname()))
               ->setFirstname(strtolower($client->getFirstname()));

        $this->manager->persist($client);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateClient(): void
    {
        $this->manager->flush();
    }

    public function removeClient(Client $client): void
    {
        $this->manager->remove($client);
        $this->manager->flush();
    }
}
