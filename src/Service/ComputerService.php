<?php

namespace App\Service;

use App\Entity\Client;
use App\Entity\Computer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ComputerService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistComputer(Computer $computer, Client $client): void
    {
        $computer->setDate(new DateTime('now'))
                 ->setClient($client);

        $this->manager->persist($computer);
        $this->manager->flush();
        $this->flash->add('sucess', 'You are in the database');
    }

    public function updateComputer(): void
    {
        $this->manager->flush();
    }

    public function removeComputer(Computer $computer): void
    {
        $this->manager->remove($computer);
        $this->manager->flush();
    }
}
