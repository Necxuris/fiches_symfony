<?php

namespace App\Service;

use App\Entity\Crowd;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CrowdService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCrowd(Crowd $crowd): void
    {
        $this->manager->persist($crowd);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateCrowd(): void
    {
        $this->manager->flush();
    }

    public function removeCrowd(Crowd $crowd): void
    {
        $this->manager->remove($crowd);
        $this->manager->flush();
    }
}
