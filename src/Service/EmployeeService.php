<?php

namespace App\Service;

use App\Entity\Employee;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EmployeeService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistEmployee(Employee $employee, UserPasswordHasherInterface $passwordHasher): void
    {
        $plainTextPassword = $employee->getPassword();
        $hashedPassword = $passwordHasher->hashPassword(
            $employee,
            $plainTextPassword
        );
        $employee->setPassword($hashedPassword);
        $employee->setCreationDate(new DateTime('now'))
                 ->setPicture('Magnifique image')
                 ->setInitials($employee->getLastname()[0] . $employee->getFirstname()[0]);

        $this->manager->persist($employee);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function removeEmployee(Employee $employee): void
    {
        $this->manager->remove($employee);
        $this->manager->flush();
    }
}
