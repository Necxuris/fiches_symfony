<?php

namespace App\Service;

use App\Entity\Computer;
use App\Entity\Employee;
use App\Entity\Fixing;
use App\Repository\ServiceRepository;
use Bluerhinos\phpMQTT;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class FixingService
{
    private $manager;
    private $flash;
    private $alertService;

    public function __construct(
        EntityManagerInterface $manager,
        FlashBagInterface $flash,
        AlertService $alertService
    ) {
        $this->manager = $manager;
        $this->flash = $flash;
        $this->alertService = $alertService;
    }

    public function persistFixing(
        Fixing $fixing,
        Employee $employee,
        Computer $computer,
        MercureService $mercureService
    ): void {
        $date = new DateTime('now');
        $fixing->setCreationDateTime($date)
               ->setComputer($computer)
               ->setCreationEmployee($employee)
               ->setStatus('Pris en charge')
               ->setComment(
                   'Status§'
                   . $employee->getInitials()
                   . ' - '
                   . $fixing->getProblem()
                   . '§'
                   . $date->format('d-m-Y H:i:s')
               )
               ->setWarantly(false)
               ->setComputerInShop(true);
        ;
        $this->manager->persist($fixing);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
        $print = new PrintService();
        $print->createTicket($fixing);
        $mercureService->sendNewFixing($fixing);
    }

    public function updateFixing(): void
    {
        $this->manager->flush();
    }

    public function removeFixing(Fixing $fixing): void
    {
        $this->manager->remove($fixing);
        $this->manager->flush();
    }

    public function addComment(Fixing $fixing, Employee $employee, object $json)
    {
        $date = new DateTime('now');

        $fixing->setComment(
            $fixing->getComment()
            . '¤Commentaire§'
            . $employee->getInitials()
            . ' - '
            . $json->comment
            . '§'
            . $date->format('d-m-Y H:i:s')
        );
        if ($fixing->getStatus() == 'Pris en charge') {
            $fixing->setStatus('En cours');
        }
        $this->manager->flush();
    }

    public function finishFixing(Fixing $fixing, Employee $employee, array $form): void
    {
        $date = new DateTime('now');

        $fixing->setComment(
            $fixing->getComment()
            . '¤Status§'
            . $employee->getInitials()
            . ' - Fini : '
            . $form['bilan']
            . '§'
            . $date->format('d-m-Y H:i:s')
        );
        $fixing->setStatus('Fini');
        $fixing->setEndEmployee($employee);
        $fixing->setOrderNumber($form['orderNumber']);
        $fixing->setEndDateTime($date);
        $this->manager->flush();
        if (isset($form['alertPhone']) && $form['alertPhone'] == 'on') {
            $this->alertService->alertMQTT(
                $fixing->getComputer()->getClient()->getPhone(),
                'finish'
            );
        }
    }

    public function waitFixing(Fixing $fixing, Employee $employee, array $form): void
    {
        $date = new DateTime('now');

        if ($fixing->getStatus() != 'Attente') {
            $fixing->setComment(
                $fixing->getComment()
                . '¤Status§'
                . $employee->getInitials()
                . ' - Attente : '
                . $form['reasons']
                . '§'
                . $date->format('d-m-Y H:i:s')
            );
            $fixing->setStatus('Attente');
        } else {
            $fixing->setComment(
                $fixing->getComment()
                . '¤Status§'
                . $employee->getInitials()
                . ' - En cours : '
                . $form['reasons']
                . '§'
                . $date->format('d-m-Y H:i:s')
            );
            $fixing->setStatus('En cours');
        }
        if (isset($form['computerInShop'])) {
            $fixing->setComputerInShop($form['computerInShop'] == "true");
        } else {
            $fixing->setComputerInShop(true);
        }
        $this->manager->flush();
        if (isset($form['alertClient']) && $form['alertClient'] == "true") {
            $this->alertService->alertMQTT(
                $fixing->getComputer()->getClient()->getPhone(),
                'wait'
            );
            $this->alertService->alertMail(
                $fixing->getComputer()->getClient()->getMail(),
                'wait'
            );
        }
    }

    public function fixingToWarantly(Fixing $fixing, Employee $employee, array $form): void
    {
        $date = new DateTime('now');

        $fixing->setComment(
            $fixing->getComment()
            . '¤Status§'
            . $employee->getInitials()
            . ' - Rendu§'
            . $date->format('d-m-Y H:i:s')
        );
        $fixing->setPaid($form['paid'] == "true");
        $fixing->setEndDateTime($date);
        $fixing->setWarantly(true);
        $fixing->setComputerInShop(false);
        $fixing->setOrderNumber(intval($form['receipt']));
        $this->manager->flush();
        $this->flash->add('warantly_success', 'Dépannage terminé avec succès');
    }

    public function pickUpWarantly(
        Fixing $fixing,
        Employee $employee,
        array $form,
        ServiceRepository $serviceRepository,
        RecordsFixingService $recordsFixingService
        // MercureService $mercureService
    ): void {
        $date = new DateTime('now');

        $recordsFixingService->persistRecordsFixing(
            $fixing,
            $fixing->getComputer()->getClient()->getAllowDataStorage()
        );
        $fixing->setCreationDateTime($date)
               ->setStatus('Pris en charge')
               ->setEndDateTime(null)
               ->setCreationEmployee($employee)
               ->setStart($form['start'] == "true")
               ->setOs($form['os'] == "true")
               ->setEndEmployee(null)
               ->setComputerInShop(true)
               ->setWarantly(false)
               ->setProblem($form['reason']);
        $fixing->setComment(
            $fixing->getComment()
            . '¤Status§'
            . $employee->getInitials()
            . ' - Retour garantie : '
            . $form['reason']
            . '§'
            . $date->format('d-m-Y H:i:s')
        );
        foreach ($fixing->getServices() as $service) {
            $fixing->removeService($service);
        }
        foreach ($fixing->getOrders() as $order) {
            $fixing->removeOrder($order);
        }
        $fixing->addService($serviceRepository->findOneBy(['number' => 0]));
        $this->manager->flush();
        $this->flash->add('warantly_pickup_success', 'Prise charge éffectuée avec succès');
        $print = new PrintService();
        $print->removeTicket($fixing);
        // $mercureService->sendNewFixing($fixing);
    }
}
