<?php

namespace App\Service;

use App\Entity\FixingTool;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class FixingToolService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistFixingTool(FixingTool $fixingTool)
    {
        $this->manager->persist($fixingTool);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateAccount(): void
    {
        $this->manager->flush();
    }

    public function removeFixingTool(FixingTool $fixingTool)
    {
        $this->manager->remove($fixingTool);
    }
}
