<?php

namespace App\Service;

use App\Entity\Material;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class MaterialService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistMaterial(Material $material): void
    {
        $this->manager->persist($material);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateMaterial(): void
    {
        $this->manager->flush();
    }

    public function removeMaterial(Material $material): void
    {
        $this->manager->remove($material);
        $this->manager->flush();
    }
}
