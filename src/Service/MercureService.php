<?php

namespace App\Service;

use App\Entity\Employee;
use App\Entity\Fixing;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;

class MercureService
{
    private $hub;

    public function __construct(HubInterface $hub)
    {
        $this->hub = $hub;
    }

    public function sendNewFixing(Fixing $fixing)
    {
        $computer = $fixing->getComputer();

        $update = new Update(
            'display',
            json_encode([
                'function' => 0,
                'id' => $fixing->getId(),
                'name' => $computer->getClient()->getLastname() . ' ' . $computer->getClient()->getFirstname(),
                'type' => $computer->getType(),
                'model' => $computer->getBrand() . ' ' . $computer->getModel(),
                'tech' => $fixing->getCreationEmployee()->getInitials(),
                'status' => $fixing->getStatus(),
                'date' => $fixing->getCreationDateTime()->format('d-m-Y')
            ])
        );
        //$this->hub->publish($update);
    }

    public function sendRemoveFixing(Fixing $fixing)
    {
        $update = new Update(
            'display',
            json_encode([
                'function' => 1,
                'id' => $fixing->getId()
            ])
        );
        //$this->hub->publish($update);
    }

    public function sendUpdateStatusFixing(Fixing $fixing, Employee $employee)
    {
        $update = new Update(
            'display',
            json_encode([
                'function' => 2,
                'id' => $fixing->getId(),
                'status' => $fixing->getStatus(),
                'tech' => $employee->getInitials()
            ])
        );
        //$this->hub->publish($update);
    }

    public function sendUpdateTechFixing(Fixing $fixing, Employee $employee)
    {
        $update = new Update(
            'display',
            json_encode([
                'function' => 3,
                'id' => $fixing->getId(),
                'tech' => $employee->getInitials()
            ])
        );
        //$this->hub->publish($update);
    }
}
