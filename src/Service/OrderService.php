<?php

namespace App\Service;

use App\Entity\Employee;
use App\Entity\Fixing;
use App\Entity\Order;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class OrderService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistOrder(Order $order, DateTime $date, Fixing $fixing, Employee $employee): void
    {
        $order->setDate($date)
              ->setFixing($fixing)
              ->setEmployee($employee)
        ;

        $this->manager->persist($order);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateOrder(): void
    {
        $this->manager->flush();
    }

    public function removeOrder(Order $order): void
    {
        $this->manager->remove($order);
        $this->manager->flush();
    }
}
