<?php

namespace App\Service;

use App\Entity\Fixing;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\VarDumper\VarDumper;

class PrintService
{
    public function removeTicket(Fixing $fixing)
    {
        $filesystem = new Filesystem();
        $filesystem->remove('tickets/ticket_fixing_' . $fixing->getId() . '.txt');
        $this->createTicket($fixing);
    }

    public function printTicket(int $id): void
    {
        $processPrint = new Process(["lpr", "-o", "raw", "tickets/ticket_fixing_" . $id .  ".txt"]);
        $processPrint->run();
    }

    public function createTicket(Fixing $fixing)
    {
        $filesystem = new Filesystem();
        $filesystem->touch('tickets/ticket_fixing_' . $fixing->getId() . '.txt');
        $client = $fixing->getComputer()->getClient();
        $phone = wordwrap($client->getPhone(), 2, " ", 1);

        function tronque( $chaine, $max ) {
            if(strlen($chaine)>=$max){// Nombre de caractère
                $chaine=substr($chaine,0,$max); // Met la portion de chaine dans $chaine
                $espace=strrpos($chaine," "); // position du dernier espace 
                if($espace) // test si il ya un espace
                    $chaine=substr($chaine,0,$espace); // si ya 1 espace, coupe de nouveau la chaine
                    $chaine .= '...';// Ajoute ... à la chaine
                }
            return $chaine;
        }

        function split($chaine, $range){
            $arr = str_split($chaine,$range);
            $boucle = 1;
            $comm = "";

            foreach( $arr as $value ){
                if($boucle == 1){
                    $comm = "^FO50,167^FH^FD" .$value. "^FS";
                    $boucle ++;
                }elseif($boucle == 2){
                    $comm .= "^FO50,188^FH^FD" .$value. "^FS";
                    $boucle ++;
                }elseif($boucle == 3){
                    $comm .= "^FO50,208^FH^FD" .$value. "^FS";
                    $boucle ++;
                }
            }

            return $comm;
        }

        $comment = $fixing->getProblem();
        $comment = tronque($comment,165);
        $comment = split($comment, 55);

        $materiels = $fixing->getMaterials();
        $chaine = "";
        $i = 0;
        foreach($materiels as $materiel){
            if($i==0){
                $chaine = $materiel->getName()."";
            }else{
                $chaine = "".$chaine."-".$materiel->getName()."";
            }
            $i++; 
        }



        $filesystem->appendToFile(
            'tickets/ticket_fixing_' . $fixing->getId() . '.txt',
            "^XA
            ^CI28

            ^CF0,25
            ^FO50,15^FD"
            . $client->getLastname() . " " . $client->getFirstname() . 
            "^FS
            ^CF0,30
            ^FO50,45^FD"
            . $phone .
            "^FS

             ^FO50,70^GB520,3,3^FS

             ^CF0,20
             ^FO50,80^FDMatériel associé : ^FS

             ^CF0,20
             ^FO50,100^FD".$chaine."^FS


             ^FO50,135^GB520,3,3^FS
           
            ^CF0,25
            ^FO50,143^FDMdp :"
            . $fixing->getComputer()->getPassword() .
            " ^FS
            ^CF0,20

            ^FO50,167^FH^FD". $comment ."^FS
            
            ^FO50,230^GB520,3,3^FS
            
            ^FO50,235^BQN,2,4^FD   http://serveur-fiches.fr/fix/fixing/" . $fixing->getId() . "^FS
            ^CF0,20
            ^FO190,242^GB370,148,3^FS
            ^FO200,250^FDSignature client:^FS
            ^CF0,20
            ^FO50,369^FDN-". $fixing->getId() ."^FS
            ^CF0,17
            ^FO200,365^FDAccepte les conditions et tarifs en vigueur.^FS
            
            ^XZ"
        );
        $this->printTicket($fixing->getId());
    }
}