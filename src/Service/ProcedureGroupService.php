<?php

namespace App\Service;

use App\Entity\ProcedureGroup;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ProcedureGroupService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistProcedureGroup(ProcedureGroup $procedureGroup): void
    {
        $this->manager->persist($procedureGroup);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateProcedureGroup(): void
    {
        $this->manager->flush();
    }

    public function saveOrder(ProcedureGroup $procedureGroup, array $json): void
    {
        $max = count($json);

        for ($i = 1; $i <= $max; $i++) {
            foreach ($procedureGroup->getProcedures() as $procedure) {
                if ($procedure->getId() == $json[$i - 1]) {
                    $procedure->setProcedureGroupOrder($i);
                }
            }
        }
        $this->manager->flush();
    }

    public function removeProceduregroup(ProcedureGroup $procedureGroup): void
    {
        foreach ($procedureGroup->getProcedures() as $procedure) {
            $procedure->setProcedureGroup(null);
            $procedure->setProcedureGroupOrder(null);
        }
        $this->manager->remove($procedureGroup);
        $this->manager->flush();
    }

    public function getElementsFromProcedure(array $procedures, ProcedureService $procedureService): array
    {
        $procedureElements = [];
        for ($i = 0; $i < count($procedures); $i++) {
            array_push($procedureElements, $procedureService->getElements($procedures[$i]->getElements()));
        }
        return $procedureElements;
    }
}
