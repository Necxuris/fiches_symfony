<?php

namespace App\Service;

use App\Entity\Procedure;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ProcedureService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistProcedure(object $data): void
    {
        $procedure = new Procedure();

        $procedure->setName($data->name);
        $procedure->setElements($data->data);
        $this->manager->persist($procedure);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateProcedure(object $data, Procedure $procedure): void
    {
        $procedure->setName($data->name);
        $procedure->setElements($data->data);
        $this->manager->flush();
    }

    public function removeProcedure(Procedure $procedure)
    {
        $this->manager->remove($procedure);
        $this->manager->flush();
    }

    public function getElements(array $procedureElements)
    {
        $elements = [];
        for ($i = 0; $i < count($procedureElements); $i++) {
            array_push($elements, array('type' => $procedureElements[$i][0], 'name' => $procedureElements[$i][1]));
        }
        return ($elements);
    }
}
