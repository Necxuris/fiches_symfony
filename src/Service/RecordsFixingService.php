<?php

namespace App\Service;

use App\Entity\Fixing;
use App\Entity\RecordsFixing;
use App\Entity\RecordsService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class RecordsFixingService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistRecordsFixing(Fixing $fixing, bool $rgpd): void
    {
        $recordsFixing = new RecordsFixing();

        $recordsFixing->setCreationDate($fixing->getCreationDateTime())
                      ->setEndDate($fixing->getEndDateTime())
                      ->setComment($fixing->getComment())
                      ->setReceiptNumber($fixing->getOrderNumber())
                      ->setEndEmployee($fixing->getEndEmployee());

        if ($rgpd) {
            $recordsFixing->setClient($fixing->getComputer()->getClient())
                          ->setComputer($fixing->getComputer());
        } else {
            $recordsFixing->setClient(null)
                          ->setComputer(null);
        }

        foreach ($fixing->getServices() as $service) {
            $recordsService = new RecordsService();
            $recordsService->setName($service->getName())
                           ->setPrice($service->getPrice())
                           ->setType($service->getType())
                           ->setNumber($service->getNumber());
            $recordsFixing->addRecordsService($recordsService);
        }

        foreach ($fixing->getOrders() as $order) {
            $recordsService = new RecordsService();
            $recordsService->setName($order->getDescription())
                           ->setPrice($order->getPrice())
                           ->setType('Commande');
            $recordsFixing->addRecordsService($recordsService);
        }

        $this->manager->persist($recordsFixing);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateRecordsFixing(): void
    {
        $this->manager->flush();
    }

    public function removeRecordsFixing(RecordsFixing $recordsFixing): void
    {
        $this->manager->remove($recordsFixing);
        $this->manager->flush();
    }
}
