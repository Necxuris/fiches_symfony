<?php

namespace App\Service;

use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ServiceService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistService(Service $service): void
    {
        $this->manager->persist($service);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateService(): void
    {
        $this->manager->flush();
    }

    public function removeService(Service $service): void
    {
        $this->manager->remove($service);
        $this->manager->flush();
    }
}
