<?php

namespace App\Service;

use App\Entity\Stats;
use App\Repository\ClientRepository;
use App\Repository\FixingRepository;
use App\Repository\RecordsFixingRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class StatsService
{
    private $manager;
    private $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistStats(Stats $stats): void
    {
        $this->manager->persist($stats);
        $this->manager->flush();
        $this->flash->add('success', 'You are in the database');
    }

    public function updateStats(): void
    {
        $this->manager->flush();
    }

    public function removeStats(Stats $stats): void
    {
        $this->manager->remove($stats);
        $this->manager->flush();
    }

    public function getStatsFromMonth(
        DateTime $date,
        ClientRepository $clientRepository,
        FixingRepository $fixingRepository,
        RecordsFixingRepository $recordsFixingRepository
    ): Stats {
        $stats = new Stats();
        $stats->setClients($clientRepository->getNumberClientFromMonth($date)[0][1]);
        $fixings = $fixingRepository->getFixingFromMonth($date);
        $recordFixings = $recordsFixingRepository->getRecordsFixingsFromMonth($date);
        $stats->setFixings(count($fixings) + count($recordFixings));

        $labor = 0;
        $total = 0;

        foreach ($fixings as $fixing) {
            foreach ($fixing->getServices() as $service) {
                if ($service->getType() == 'P' || $service->getType() == 'F') {
                    $labor += $service->getPrice();
                }
                $total += $service->getPrice();
            }
            foreach ($fixing->getOrders() as $order) {
                $total += $order->getPrice();
            }
        }

        $stats->setLabor($labor);
        $stats->setTotal($total);
        return $stats;
    }

    public function statsBackoffice(Stats $statsMonth, Stats $previousMonth): array
    {
        $material = $statsMonth->getTotal() - $statsMonth->getLabor();
        $previousMaterial = $previousMonth->getTotal() - $previousMonth->getLabor();
        $moy = ($statsMonth->getFixings() != 0)
            ? ($statsMonth->getTotal() / $statsMonth->getFixings())
            : 0
        ;
        $previousMoy = ($previousMonth->getFixings() != 0)
            ? ($previousMonth->getTotal() / $previousMonth->getFixings())
            : 0
        ;
        $laborMoy = ($statsMonth->getFixings() != 0)
            ? ($statsMonth->getLabor() / $statsMonth->getFixings())
            : 0
        ;
        $previousLaborMoy = ($previousMonth->getFixings() != 0)
            ? ($previousMonth->getLabor() / $previousMonth->getFixings())
            : 0
        ;

        return array(
            'client' => array(
                'new' => $statsMonth->getClients(),
                'rate' => ($previousMonth->getClients() != 0)
                    ? (int)((($statsMonth->getClients() / $previousMonth->getClients()) - 1) * 100)
                    : $statsMonth->getClients() * 100
            ),
            'fixing' => array(
                'new' => $statsMonth->getFixings(),
                'rate' => ($previousMonth->getFixings() != 0)
                    ? (int)((($statsMonth->getFixings() / $previousMonth->getFixings()) - 1) * 100)
                    : $statsMonth->getFixings() * 100
            ),
            'labor' => array(
                'new' => $statsMonth->getLabor(),
                'rate' => ($previousMonth->getLabor() != 0)
                    ? (int)((($statsMonth->getLabor() / $previousMonth->getLabor()) - 1) * 100)
                    : $statsMonth->getLabor() * 100
            ),
            'total' => array(
                'new' => $statsMonth->getTotal(),
                'rate' => ($previousMonth->getTotal() != 0)
                    ? (int)((($statsMonth->getTotal() / $previousMonth->getTotal()) - 1) * 100)
                    : $statsMonth->getTotal() * 100
            ),
            'material' => array(
                'new' => $material,
                'rate' => ($previousMaterial != 0)
                    ? (int)((($material / $previousMaterial) - 1) * 100)
                    : $material * 100
            ),
            'moy' => array(
                'new' => $moy,
                'rate' => ($previousMoy != 0)
                    ? (int)((($moy / $previousMoy) - 1) * 100)
                    : $moy * 100
            ),
            'laborMoy' => array(
                'new' => $laborMoy,
                'rate' => ($previousLaborMoy != 0)
                    ? (int)((($laborMoy / $previousLaborMoy) - 1) * 100)
                    : $laborMoy * 100
            )
        );
    }
}
