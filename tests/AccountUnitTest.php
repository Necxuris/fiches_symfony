<?php

namespace App\Tests;

use App\Entity\Account;
use App\Entity\Computer;
use App\Entity\Employee;
use PHPUnit\Framework\TestCase;

class AccountUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $account = new Account();
        $computer = new Computer();
        $employee = new Employee();

        $account->setLabel('bitdef')
                ->setLogin('test@test.com')
                ->setPassword('password')
                ->setMoreInfo('info')
                ->setComputer($computer)
                ->setEmployee($employee);
        
        $this->assertTrue($account->getLabel() === 'bitdef');
        $this->assertTrue($account->getLogin() === 'test@test.com');
        $this->assertTrue($account->getPassword() === 'password');
        $this->assertTrue($account->getMoreInfo() === 'info');
        $this->assertTrue($account->getComputer() === $computer);
        $this->assertTrue($account->getEmployee() === $employee);
    }

    public function testIsFalse(): void
    {
        $account = new Account();
        $computer = new Computer();
        $employee = new Employee();

        $account->setLabel('bitdef')
                ->setLogin('test@test.com')
                ->setPassword('password')
                ->setMoreInfo('info')
                ->setComputer($computer)
                ->setEmployee($employee);
        
        $this->assertFalse($account->getLabel() === 'fbitdef');
        $this->assertFalse($account->getLogin() === 'ftest@test.com');
        $this->assertFalse($account->getPassword() === 'fpassword');
        $this->assertFalse($account->getMoreInfo() === 'finfo');
        $this->assertFalse($account->getComputer() === new Computer());
        $this->assertFalse($account->getEmployee() === new Employee());
    }

    public function testIsEmpty(): void
    {
        $account = new Account();

        $this->assertEmpty($account->getLabel());
        $this->assertEmpty($account->getLogin());
        $this->assertEmpty($account->getPassword());
        $this->assertEmpty($account->getMoreInfo());
        $this->assertEmpty($account->getComputer());
        $this->assertEmpty($account->getEmployee());
    }
}
