<?php

namespace App\Tests;

use App\Entity\Action;
use App\Entity\Employee;
use App\Entity\Fixing;
use PHPUnit\Framework\TestCase;

class ActionUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $action = new Action();
        $fixing = new Fixing();
        $employee = new Employee();

        $action->setProcedureNumber(12)
               ->setActionNumber(34)
               ->setFixing($fixing)
               ->setEmployee($employee)
               ->setValue('test')
               ->setProcedureGroupNumber(12)
               ->setName('test');
        
        $this->assertTrue($action->getProcedureNumber() === 12);
        $this->assertTrue($action->getActionNumber() === 34);
        $this->assertTrue($action->getFixing() === $fixing);
        $this->assertTrue($action->getEmployee() === $employee);
        $this->assertTrue($action->getValue() === 'test');
        $this->assertTrue($action->getProcedureGroupNumber() === 12);
        $this->assertTrue($action->getName() === 'test');
    }
    public function testIsFalse(): void
    {
        $action = new Action();
        $fixing = new Fixing();
        $employee = new Employee();

        $action->setProcedureNumber(12)
               ->setActionNumber(34)
               ->setFixing($fixing)
               ->setEmployee($employee)
               ->setValue('test')
               ->setProcedureGroupNumber(12)
               ->setName('test');
        
        $this->assertFalse($action->getProcedureNumber() === 123);
        $this->assertFalse($action->getActionNumber() === 345);
        $this->assertFalse($action->getFixing() === new Fixing());
        $this->assertFalse($action->getEmployee() === new Employee());
        $this->assertFalse($action->getValue() === 'teste');
        $this->assertFalse($action->getProcedureGroupNumber() === 123);
        $this->assertFalse($action->getName() === 'teste');
    }

    public function testIsEmpty(): void
    {
        $action = new Action();

        $this->assertEmpty($action->getProcedureNumber());
        $this->assertEmpty($action->getActionNumber());
        $this->assertEmpty($action->getFixing());
        $this->assertEmpty($action->getEmployee());
        $this->assertEmpty($action->getName());
        $this->assertEmpty($action->getProcedureGroupNumber());
        $this->assertEmpty($action->getValue());
    }
}
