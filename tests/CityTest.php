<?php

namespace App\Tests;

use App\Entity\City;
use App\Entity\Postcode;
use PHPUnit\Framework\TestCase;

class CityTest extends TestCase
{
    public function testIsTrue(): void
    {
        $city = new City();
        $postcode = new Postcode();

        $city->setName('test')
             ->setPostcode($postcode);

        
        $this->assertTrue($city->getName() === 'test');
        $this->assertTrue($city->getPostcode() === $postcode);
    }

    public function testIsFalse(): void
    {
        $city = new City();
        $postcode = new Postcode();

        $city->setName('test')
             ->setPostcode($postcode);

        
        $this->assertFalse($city->getName() === 'teste');
        $this->assertFalse($city->getPostcode() === new Postcode());
    }

    public function testIsEmpty(): void
    {
        $city = new City();
        
        $this->assertEmpty($city->getName());
        $this->assertEmpty($city->getPostcode());
    }
}
