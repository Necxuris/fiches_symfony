<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Computer;
use App\Entity\RecordsFixing;
use App\Entity\WarantyFixing;
use DateTime;
use PHPUnit\Framework\TestCase;

class ClientUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new Client();
        $computer = new Computer();
        $recordsFixing = new RecordsFixing();
        $date = new DateTime('now');

        $user->setMail('true@test.com')
             ->setFirstname('prenom')
             ->setLastname('nom')
             ->setAddress('12 rue du test')
             ->setCity('Epinal')
             ->setPostcode('11111')
             ->setPhone('0123456789')
             ->setNewsletter(true)
             ->setAllowDataStorage(true)
             ->setIdentificationCode(651436)
             ->addComputer($computer)
             ->addRecordsFixing($recordsFixing)
             ->setCreationDate($date);

        
        $this->assertTrue($user->getMail() === 'true@test.com');
        $this->assertTrue($user->getFirstname() === 'prenom');
        $this->assertTrue($user->getLastname() === 'nom');
        $this->assertTrue($user->getAddress() === '12 rue du test');
        $this->assertTrue($user->getCity() === 'Epinal');
        $this->assertTrue($user->getPostcode() === '11111');
        $this->assertTrue($user->getPhone() === '0123456789');
        $this->assertTrue($user->getNewsletter() === true);
        $this->assertTrue($user->getAllowDataStorage() === true);
        $this->assertTrue($user->getIdentificationCode() === 651436);
        $this->assertTrue($user->getComputers()[0] === $computer);
        $this->assertTrue($user->getRecordsFixings()[0] === $recordsFixing);
        $this->assertTrue($user->getCreationDate() === $date);
    }

    public function testIsFalse(): void
    {
        $user = new Client();
        $computer = new Computer();
        $recordsFixing = new RecordsFixing();
        $date = new DateTime('now');

        $user->setMail('true@test.com')
             ->setFirstname('prenom')
             ->setLastname('nom')
             ->setAddress('12 rue du test')
             ->setCity('Epinal')
             ->setPostcode('11111')
             ->setPhone('0123456789')
             ->setNewsletter(true)
             ->setAllowDataStorage(true)
             ->setIdentificationCode(651436)
             ->addComputer($computer)
             ->addRecordsFixing($recordsFixing)
             ->setCreationDate($date);
        
        $this->assertFalse($user->getMail() === 'false@test.com');
        $this->assertFalse($user->getFirstname() === 'fprenom');
        $this->assertFalse($user->getLastname() === 'fnom');
        $this->assertFalse($user->getAddress() === 'f12 rue du test');
        $this->assertFalse($user->getCity() === 'fEpinal');
        $this->assertFalse($user->getPostcode() === 'f11111');
        $this->assertFalse($user->getPhone() === 'f0123456789');
        $this->assertFalse($user->getNewsletter() === false);
        $this->assertFalse($user->getAllowDataStorage() === false);
        $this->assertFalse($user->getIdentificationCode() === 34651436);
        $this->assertFalse($user->getComputers()[0] === new Computer());
        $this->assertFalse($user->getRecordsFixings()[0] === new RecordsFixing());
        $this->assertFalse($user->getCreationDate() === new DateTime());
    }

    public function testIsEmpty(): void
    {
        $user = new Client();

        $this->assertEmpty($user->getMail() === 'false@test.com');
        $this->assertEmpty($user->getFirstname() === 'fprenom');
        $this->assertEmpty($user->getLastname() === 'fnom');
        $this->assertEmpty($user->getAddress() === 'f12 rue du test');
        $this->assertEmpty($user->getCity() === 'fEpinal');
        $this->assertEmpty($user->getPostcode() === 'f11111');
        $this->assertEmpty($user->getPhone() === 'f0123456789');
        $this->assertEmpty($user->getNewsletter() === false);
        $this->assertEmpty($user->getAllowDataStorage() === false);
        $this->assertEmpty($user->getIdentificationCode() === 34651436);
        $this->assertEmpty($user->getComputers());
        $this->assertEmpty($user->getRecordsFixings());
        $this->assertEmpty($user->getCreationDate());
    }
}
