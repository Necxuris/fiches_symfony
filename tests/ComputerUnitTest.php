<?php

namespace App\Tests;

use App\Entity\Account;
use App\Entity\Client;
use App\Entity\Computer;
use App\Entity\RecordsFixing;
use App\Entity\WarantyFixing;
use DateTime;
use PHPUnit\Framework\TestCase;

class ComputerUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $computer = new Computer();
        $client = new Client();
        $date = new DateTime();
        $account = new Account();
        $recordsFixing = new RecordsFixing();

        $computer->setBrand('HP')
                 ->setBrandPicture('urlsublime')
                 ->setType('fixe')
                 ->setModel('TPN452')
                 ->setPassword('password')
                 ->setDate($date)
                 ->setClient($client)
                 ->addAccount($account)
                 ->addRecordsFixing($recordsFixing);

        $this->assertTrue($computer->getBrand() === 'HP');
        $this->assertTrue($computer->getBrandPicture() === 'urlsublime');
        $this->assertTrue($computer->getType() === 'fixe');
        $this->assertTrue($computer->getModel() === 'TPN452');
        $this->assertTrue($computer->getPassword() === 'password');
        $this->assertTrue($computer->getDate() === $date);
        $this->assertTrue($computer->getClient() === $client);
        $this->assertTrue($computer->getAccounts()[0] === $account);
        $this->assertTrue($computer->getRecordsFixings()[0] === $recordsFixing);
    }

    public function testIsFalse(): void
    {
        $computer = new Computer();
        $client = new Client();
        $date = new DateTime();
        $account = new Account();
        $recordsFixing = new RecordsFixing();

        $computer->setBrand('HP')
                 ->setBrandPicture('urlsublime')
                 ->setType('fixe')
                 ->setModel('TPN452')
                 ->setPassword('password')
                 ->setDate($date)
                 ->setClient($client)
                 ->addAccount($account)
                 ->addRecordsFixing($recordsFixing);

        $this->assertFalse($computer->getBrand() === 'fHP');
        $this->assertFalse($computer->getBrandPicture() === 'furlsublime');
        $this->assertFalse($computer->getType() === 'ffixe');
        $this->assertFalse($computer->getModel() === 'fTPN452');
        $this->assertFalse($computer->getPassword() === 'fpassword');
        $this->assertFalse($computer->getDate() === new DateTime());
        $this->assertFalse($computer->getClient() === new Client);
        $this->assertFalse($computer->getAccounts()[0] === new Account());
        $this->assertFalse($computer->getRecordsFixings()[0] === new RecordsFixing());
    }

    public function testIsEmpty(): void
    {
        $computer = new Computer();

        $this->assertEmpty($computer->getBrand());
        $this->assertEmpty($computer->getBrandPicture());
        $this->assertEmpty($computer->getType());
        $this->assertEmpty($computer->getModel());
        $this->assertEmpty($computer->getPassword());
        $this->assertEmpty($computer->getDate());
        $this->assertEmpty($computer->getClient());
        $this->assertEmpty($computer->getAccounts());
        $this->assertEmpty($computer->getRecordsFixings());
    }
}
