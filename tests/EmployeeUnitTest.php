<?php

namespace App\Tests;

use App\Entity\Employee;
use App\Entity\Fixing;
use App\Entity\Order;
use DateTime;
use PHPUnit\Framework\TestCase;

class EmployeeUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $employee = new Employee();
        $date = new DateTime();
        $fixing = new Fixing();
        $order = new Order();
        $roles[] = 'ROLE_ADMIN';

        $employee->setEmail('true@test.com')
                 ->setFirstname('prenom')
                 ->setLastname('nom')
                 ->setRoles($roles)
                 ->setPicture('urlmagnifique')
                 ->setInitials('EZ')
                 ->setPassword('password')
                 ->setCreationDate($date)
                 ->addOrder($order)
                 ->addFixingsTaken($fixing);

        $this->assertTrue($employee->getEmail() === 'true@test.com');
        $this->assertTrue($employee->getFirstname() === 'prenom');
        $this->assertTrue($employee->getLastname() === 'nom');
        $this->assertTrue($employee->getRoles() === $roles);
        $this->assertTrue($employee->getPicture() === 'urlmagnifique');
        $this->assertTrue($employee->getInitials() === 'EZ');
        $this->assertTrue($employee->getPassword() === 'password');
        $this->assertTrue($employee->getCreationDate() === $date);
        $this->assertTrue($employee->getOrders()[0] === $order);
        $this->assertTrue($employee->getFixingsTaken()[0] === $fixing);
    }

    public function testIsFalse(): void
    {
        $employee = new Employee();
        $date = new DateTime();
        $fixing = new Fixing();
        $order = new Order();

        $employee->setEmail('true@test.com')
                 ->setFirstname('prenom')
                 ->setLastname('nom')
                 ->setRoles(['ROLES_ADMIN'])
                 ->setPicture('urlmagnifique')
                 ->setInitials('EZ')
                 ->setPassword('password')
                 ->setCreationDate($date)
                 ->addOrder($order)
                 ->addFixingsTaken($fixing);

        $this->assertFalse($employee->getEmail() === 'ftrue@test.com');
        $this->assertFalse($employee->getFirstname() === 'fprenom');
        $this->assertFalse($employee->getLastname() === 'fnom');
        $this->assertFalse($employee->getRoles() === ['fROLES_ADMIN']);
        $this->assertFalse($employee->getPicture() === 'furlmagnifique');
        $this->assertFalse($employee->getInitials() === 'fEZ');
        $this->assertFalse($employee->getPassword() === 'fpassword');
        $this->assertFalse($employee->getCreationDate() === new DateTime());
        $this->assertFalse($employee->getOrders()[0] === new Order());
        $this->assertFalse($employee->getFixingsTaken()[0] === new Fixing());
     }

    public function testIsEmpty(): void
    {
        $employee = new Employee();

        $this->assertEmpty($employee->getEmail());
        $this->assertEmpty($employee->getFirstname());
        $this->assertEmpty($employee->getLastname());
        $this->assertEmpty($employee->getPicture());
        $this->assertEmpty($employee->getInitials());
        $this->assertEmpty($employee->getPassword());
        $this->assertEmpty($employee->getCreationDate());
        $this->assertEmpty($employee->getOrders());
        $this->assertEmpty($employee->getFixingsTaken());
    }
}
