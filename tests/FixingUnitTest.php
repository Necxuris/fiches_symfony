<?php

namespace App\Tests;

use App\Entity\Action;
use App\Entity\Computer;
use App\Entity\Employee;
use App\Entity\Fixing;
use App\Entity\Material;
use App\Entity\Order;
use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use App\Entity\Service;
use App\Entity\ServiceFixing;
use DateTime;
use PHPUnit\Framework\TestCase;

class FixingUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $fixing = new Fixing();
        $computer = new Computer();
        $employee = new Employee();
        $date = new DateTime();
        $material = new Material();
        $action = new Action();
        $order = new Order();
        $procedure = new Procedure();
        $service = new Service();
        $procedureGroup = new ProcedureGroup();

        $fixing->setCreationDateTime($date)
               ->setComment('comment')
               ->setStatus('En cours')
               ->setEndDateTime($date)
               ->setComputer($computer)
               ->setEndEmployee($employee)
               ->setOrderNumber(1234)
               ->addMaterial($material)
               ->addAction($action)
               ->addOrder($order)
               ->setCreationEmployee($employee)
               ->setStart(true)
               ->setOs(true)
               ->setDamage('test')
               ->addProcedure($procedure)
               ->addService($service)
               ->setWarantly(true)
               ->setPaid(true)
               ->setComputerInShop(true)
               ->setProblem('test')
               ->addProcedureGroup($procedureGroup);

        $this->assertTrue($fixing->getCreationDateTime() === $date);
        $this->assertTrue($fixing->getComment() === 'comment');
        $this->assertTrue($fixing->getStatus() === 'En cours');
        $this->assertTrue($fixing->getEndDateTime() === $date);
        $this->assertTrue($fixing->getComputer() === $computer);
        $this->assertTrue($fixing->getEndEmployee() === $employee);
        $this->assertTrue($fixing->getOrderNumber() === 1234);
        $this->assertTrue($fixing->getMaterials()[0] === $material);
        $this->assertTrue($fixing->getActions()[0] === $action);
        $this->assertTrue($fixing->getOrders()[0] === $order);
        $this->assertTrue($fixing->getCreationEmployee() === $employee);
        $this->assertTrue($fixing->getStart() === true);
        $this->assertTrue($fixing->getOs() === true);
        $this->assertTrue($fixing->getDamage() === 'test');
        $this->assertTrue($fixing->getProcedures()[0] === $procedure);
        $this->assertTrue($fixing->getServices()[0] === $service);
        $this->assertTrue($fixing->getWarantly() === true);
        $this->assertTrue($fixing->getPaid() === true);
        $this->assertTrue($fixing->getComputerInShop() === true);
        $this->assertTrue($fixing->getProblem() === 'test');
        $this->assertTrue($fixing->getProcedureGroups()[0] === $procedureGroup);
    }

    public function testIsFalse(): void
    {
        $fixing = new Fixing();
        $computer = new Computer();
        $employee = new Employee();
        $date = new DateTime();
        $material = new Material();
        $action = new Action();
        $order = new Order();
        $procedure = new Procedure();
        $service = new Service();
        $procedureGroup = new ProcedureGroup();

        $fixing->setCreationDateTime($date)
               ->setComment('comment')
               ->setStatus('En cours')
               ->setEndDateTime($date)
               ->setComputer($computer)
               ->setEndEmployee($employee)
               ->setOrderNumber(1234)
               ->addMaterial($material)
               ->addAction($action)
               ->addOrder($order)
               ->setCreationEmployee($employee)
               ->setStart(true)
               ->setOs(true)
               ->setDamage('test')
               ->addProcedure($procedure)
               ->addService($service)
               ->setWarantly(true)
               ->setPaid(true)
               ->setComputerInShop(true)
               ->setProblem('test')
               ->addProcedureGroup($procedureGroup);
        
        $this->assertFalse($fixing->getCreationDateTime() === new DateTime());
        $this->assertFalse($fixing->getComment() === 'fcomment');
        $this->assertFalse($fixing->getStatus() === 'fEn cours');
        $this->assertFalse($fixing->getEndDateTime() === new DateTime());
        $this->assertFalse($fixing->getComputer() === new Computer());
        $this->assertFalse($fixing->getEndEmployee() === new Employee());
        $this->assertFalse($fixing->getOrderNumber() === 12345);
        $this->assertFalse($fixing->getMaterials()[0] === new Material());
        $this->assertFalse($fixing->getActions()[0] === new Action());
        $this->assertFalse($fixing->getOrders()[0] === new Order());
        $this->assertFalse($fixing->getCreationEmployee() === new Employee());
        $this->assertFalse($fixing->getStart() === false);
        $this->assertFalse($fixing->getOs() === false);
        $this->assertFalse($fixing->getDamage() === 'tests');
        $this->assertFalse($fixing->getProcedures()[0] === new Procedure());
        $this->assertFalse($fixing->getServices()[0] === new Service());
        $this->assertFalse($fixing->getWarantly() === false);
        $this->assertFalse($fixing->getPaid() === false);
        $this->assertFalse($fixing->getComputerInShop() === false);
        $this->assertFalse($fixing->getProblem() === 'teste');
        $this->assertFalse($fixing->getProcedureGroups()[0] === new ProcedureGroup());
    }

    public function testIsEmpty(): void
    {
        $fixing = new Fixing();

        $this->assertEmpty($fixing->getCreationDateTime());
        $this->assertEmpty($fixing->getComment());
        $this->assertEmpty($fixing->getStatus());
        $this->assertEmpty($fixing->getEndDateTime());
        $this->assertEmpty($fixing->getComputer());
        $this->assertEmpty($fixing->getEndEmployee());
        $this->assertEmpty($fixing->getOrderNumber());
        $this->assertEmpty($fixing->getMaterials());
        $this->assertEmpty($fixing->getActions());
        $this->assertEmpty($fixing->getOrders());
        $this->assertEmpty($fixing->getCreationEmployee());
        $this->assertEmpty($fixing->getOs());
        $this->assertEmpty($fixing->getStart());
        $this->assertEmpty($fixing->getDamage());
        $this->assertEmpty($fixing->getProcedures());
        $this->assertEmpty($fixing->getServices());
        $this->assertEmpty($fixing->getWarantly());
        $this->assertEmpty($fixing->getPaid());
        $this->assertEmpty($fixing->getComputerInShop());
        $this->assertEmpty($fixing->getProblem());
        $this->assertEmpty($fixing->getProcedureGroups());
    }
}
