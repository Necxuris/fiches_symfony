<?php

namespace App\Tests;

use App\Entity\Material;
use PHPUnit\Framework\TestCase;

class MaterialUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $material = new Material;

        $material->setName('test');
        
        $this->assertTrue($material->getName() === 'test');
    }

    public function testIsFalse(): void
    {
        $material = new Material;

        $material->setName('test');
        
        $this->assertFalse($material->getName() === 'ftest');
    }

    public function testIsEmpty(): void
    {
        $material = new Material;
        
        $this->assertEmpty($material->getName());
    }
}
