<?php

namespace App\Tests;

use App\Entity\Employee;
use App\Entity\Fixing;
use App\Entity\Order;
use DateTime;
use PHPUnit\Framework\TestCase;

class OrderUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $order = new Order();
        $fixing = new Fixing();
        $employee = new Employee();
        $date = new DateTime();

        $order->setDescription('description')
              ->setEshop('eshop')
              ->setDate($date)
              ->setFixing($fixing)
              ->setEmployee($employee)
              ->setPrice(45);

        $this->assertTrue($order->getDescription() === 'description');
        $this->assertTrue($order->getEshop() === 'eshop');
        $this->assertTrue($order->getDate() === $date);
        $this->assertTrue($order->getFixing() === $fixing);
        $this->assertTrue($order->getEmployee() === $employee);
        $this->assertTrue($order->getPrice() === 45);
    }

    public function testIsFalse(): void
    {
        $order = new Order();
        $fixing = new Fixing();
        $employee = new Employee();
        $date = new DateTime();

        $order->setDescription('description')
              ->setEshop('eshop')
              ->setDate($date)
              ->setFixing($fixing)
              ->setEmployee($employee)
              ->setPrice(42);

        $this->assertFalse($order->getDescription() === 'fdescription');
        $this->assertFalse($order->getEshop() === 'feshop');
        $this->assertFalse($order->getDate() === new DateTime());
        $this->assertFalse($order->getFixing() === new Fixing());
        $this->assertFalse($order->getEmployee() === new Employee());
        $this->assertFalse($order->getPrice() === 45);
    }

    public function testIsEmpty(): void
    {
        $order = new Order();

        $this->assertEmpty($order->getDescription());
        $this->assertEmpty($order->getEshop());
        $this->assertEmpty($order->getDate());
        $this->assertEmpty($order->getFixing());
        $this->assertEmpty($order->getEmployee());
        $this->assertEmpty($order->getPrice());
    }
}
