<?php

namespace App\Tests;

use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use PHPUnit\Framework\TestCase;

class ProcedureGroupTest extends TestCase
{
    public function testIsTrue(): void
    {
        $procedureGroup = new ProcedureGroup;
        $procedure = new Procedure;
        $procedureGroup->setName('test')
                       ->addProcedure($procedure);

        $this->assertTrue($procedureGroup->getName() === 'test');
        $this->assertTrue($procedureGroup->getProcedures()[0] === $procedure);
    }

    public function testIsFalse(): void
    {
        $procedureGroup = new ProcedureGroup;
        $procedure = new Procedure;
        $procedureGroup->setName('test')
                       ->addProcedure($procedure);

        $this->assertFalse($procedureGroup->getName() === 'teste');
        $this->assertFalse($procedureGroup->getProcedures() === [new Procedure]);
    }

    public function testIsEmpty(): void
    {
        $procedureGroup = new ProcedureGroup;

        $this->assertEmpty($procedureGroup->getName());
        $this->assertEmpty($procedureGroup->getProcedures());
    }

}
