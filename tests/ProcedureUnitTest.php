<?php

namespace App\Tests;

use App\Entity\Procedure;
use App\Entity\ProcedureGroup;
use PHPUnit\Framework\TestCase;

class ProcedureUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $procedure = new Procedure;
        $procedureGroup = new ProcedureGroup;
        $procedure->setName('test')
                  ->setElements(['test'])
                  ->setProcedureGroup($procedureGroup)
                  ->setProcedureGroupOrder(1);

        $this->assertTrue($procedure->getName() === 'test');
        $this->assertTrue($procedure->getElements() === ['test']);
        $this->assertTrue($procedure->getProcedureGroup() === $procedureGroup);
        $this->assertTrue($procedure->getProcedureGroupOrder() === 1);
    }

    public function testIsFalse(): void
    {
        $procedure = new procedure;
        $procedureGroup = new ProcedureGroup;
        $procedure->setName('test')
                ->setElements(['test'])
                ->setProcedureGroup($procedureGroup)
                ->setProcedureGroupOrder(1);
        
        $this->assertFalse($procedure->getName() === 'ftest');
        $this->assertFalse($procedure->getElements() === ['teste']);
        $this->assertFalse($procedure->getProcedureGroup() === new ProcedureGroup);
        $this->assertFalse($procedure->getProcedureGroupOrder() === 2);
    }

    public function testIsEmpty(): void
    {
        $procedure = new procedure;
        
        $this->assertEmpty($procedure->getName());
        $this->assertEmpty($procedure->getElements());
        $this->assertEmpty($procedure->getProcedureGroup());
        $this->assertEmpty($procedure->getProcedureGroupOrder());
    }
}
