<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Computer;
use App\Entity\Employee;
use App\Entity\RecordsFixing;
use App\Entity\RecordsService;
use DateTime;
use PHPUnit\Framework\TestCase;

class RecordsFixingUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $recordsFixing = new RecordsFixing();
        $recordsService = new RecordsService();
        $computer = new Computer();
        $client = new Client();
        $date = new DateTime();
        $employee = new Employee();

        $recordsFixing->setCreationDate($date)
                      ->setEndDate($date)
                      ->setComment('comment')
                      ->setReceiptNumber(12)
                      ->addRecordsService($recordsService)
                      ->setClient($client)
                      ->setComputer($computer)
                      ->setEndEmployee($employee);
    
        $this->assertTrue($recordsFixing->getCreationDate() === $date);
        $this->assertTrue($recordsFixing->getEndDate() === $date);
        $this->assertTrue($recordsFixing->getComment() === 'comment');
        $this->assertTrue($recordsFixing->getReceiptNumber() === 12);
        $this->assertTrue($recordsFixing->getRecordsServices()[0] === $recordsService);
        $this->assertTrue($recordsFixing->getClient() === $client);
        $this->assertTrue($recordsFixing->getComputer() === $computer);
        $this->assertTrue($recordsFixing->getEndEmployee() === $employee);
    }

    public function testIsFalse(): void
    {
        $recordsFixing = new RecordsFixing();
        $recordsService = new RecordsService();
        $computer = new Computer();
        $client = new Client();
        $date = new DateTime();
        $employee = new Employee();

        $recordsFixing->setCreationDate($date)
                      ->setEndDate($date)
                      ->setComment('comment')
                      ->setReceiptNumber(12)
                      ->addRecordsService($recordsService)
                      ->setClient($client)
                      ->setComputer($computer)
                      ->setEndEmployee($employee);
    
        $this->assertFalse($recordsFixing->getCreationDate() === new DateTime());
        $this->assertFalse($recordsFixing->getEndDate() === new DateTime());
        $this->assertFalse($recordsFixing->getComment() === 'fcomment');
        $this->assertFalse($recordsFixing->getReceiptNumber() === 123);
        $this->assertFalse($recordsFixing->getRecordsServices()[0] === new RecordsService());
        $this->assertFalse($recordsFixing->getClient() === new Client());
        $this->assertFalse($recordsFixing->getComputer() === new Computer());
        $this->assertFalse($recordsFixing->getEndEmployee() === new Employee());
    }

    public function testIsEmpty(): void
    {
        $recordsFixing = new RecordsFixing();
    
        $this->assertEmpty($recordsFixing->getCreationDate());
        $this->assertEmpty($recordsFixing->getEndDate());
        $this->assertEmpty($recordsFixing->getComment());
        $this->assertEmpty($recordsFixing->getReceiptNumber());
        $this->assertEmpty($recordsFixing->getRecordsServices());
        $this->assertEmpty($recordsFixing->getClient());
        $this->assertEmpty($recordsFixing->getComputer());
        $this->assertEmpty($recordsFixing->getEndEmployee());
    }
}
