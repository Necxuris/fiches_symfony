<?php

namespace App\Tests;

use App\Entity\RecordsFixing;
use App\Entity\RecordsService;
use App\Entity\Service;
use PHPUnit\Framework\TestCase;

class RecordsServiceUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $recordsService = new RecordsService();
        $recordsFixing = new RecordsFixing();
        $service = new Service();

        $recordsService->setName('P1')
                       ->setPrice(12)
                       ->setRecordsFixing($recordsFixing)
                       ->setType('test');

        $this->assertTrue($recordsService->getName() === 'P1');
        $this->assertTrue($recordsService->getPrice() === 12);
        $this->assertTrue($recordsService->getRecordsFixing() === $recordsFixing);
        $this->assertTrue($recordsService->getType() === 'test');
    }

    public function testIsFalse(): void
    {
        $recordsService = new RecordsService();
        $recordsFixing = new RecordsFixing();
        $service = new Service();

        $recordsService->setName('P1')
                       ->setPrice(12)
                       ->setrecordsFixing($recordsFixing)
                       ->setType('test');

        $this->assertFalse($recordsService->getName() === 'FP1');
        $this->assertFalse($recordsService->getPrice() === 112);
        $this->assertFalse($recordsService->getRecordsFixing() === new RecordsFixing());
        $this->assertFalse($recordsService->getType() === 'teste');
    }

    public function testIsEmpty(): void
    {
        $recordsService = new RecordsService();

        $this->assertEmpty($recordsService->getName());
        $this->assertEmpty($recordsService->getPrice());
        $this->assertEmpty($recordsService->getRecordsFixing());
        $this->assertEmpty($recordsService->getType());
    }
}
