<?php

namespace App\Tests;

use App\Entity\Fixing;
use App\Entity\Service;
use PHPUnit\Framework\TestCase;

class ServiceUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $service = new Service();
        $fixing = new Fixing();

        $service->setName('test')
                ->setPrice(12)
                ->setType('test')
                ->setNumber(45)
                ->addFixing($fixing);
        
        $this->assertTrue($service->getName() === 'test');
        $this->assertTrue($service->getPrice() === 12);
        $this->assertTrue($service->getType() === 'test');
        $this->assertTrue($service->getNumber() === 45);
        $this->assertTrue($service->getFixings()[0] === $fixing);
    }

    public function testIsFalse(): void
    {
        $service = new Service();
        $fixing = new Fixing();

        $service->setName('test')
                ->setPrice(12)
                ->setType('test')
                ->setNumber(45)
                ->addFixing($fixing);
        
        $this->assertFalse($service->getName() === 'ftest');
        $this->assertFalse($service->getPrice() === 123);
        $this->assertFalse($service->getType() === 'teste');
        $this->assertFalse($service->getNumber() === 456);
        $this->assertFalse($service->getFixings()[0] === new Fixing());
    }

    public function testIsEmpty(): void
    {
        $service = new Service();
        
        $this->assertEmpty($service->getName());
        $this->assertEmpty($service->getPrice());
        $this->assertEmpty($service->getType());
        $this->assertEmpty($service->getNumber());
        $this->assertEmpty($service->getFixings());
    }
}
